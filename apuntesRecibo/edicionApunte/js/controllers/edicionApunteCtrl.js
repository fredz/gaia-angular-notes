/**
 * Created by Fred on 9/22/2017.
 */
(function () {
    'use strict';

    /*global appModule */
    (appModule.lazy || appModule)
        .controller('EdicionApunteCtrl', ['$scope', 'apuntesSrv', '$window', 
        'LoadingSrv', '$filter', 'tablasGeneralesSrv',
        'comunesSrv', 'carteraSrv', 'alertaApuntesSrv', 
        '$location', '$anchorScroll', '$rootScope',
        'modalConfirmacionSrv', 'altaApuntesSrv',
        'navegacionSrv', '$state', 'CONSTANTES_GENERALES', function ($scope, apuntesSrv, $window, 
          LoadingSrv, $filter, tablasGeneralesSrv,
          comunesSrv, carteraSrv, alertaApuntesSrv, 
          $location, $anchorScroll, $rootScope,
          modalConfirmacionSrv, altaApuntesSrv,
          navegacionSrv, $state, CONSTANTES_GENERALES) {

          var needLoadCartera = false,
              codeNegocioInd = CONSTANTES_GENERALES.codeNegocioInd,
              createForm = CONSTANTES_GENERALES.createForm,
              suplementoCod = CONSTANTES_GENERALES.suplementoCod,
              nuevaProd = CONSTANTES_GENERALES.suplemenuevaProdntoCod,
              renovacionCod = CONSTANTES_GENERALES.renovacionCod,
              deRenovacionCod = CONSTANTES_GENERALES.deRenovacionCod,
              emitidoCod = CONSTANTES_GENERALES.emitidoCod,
              anuladoCod = CONSTANTES_GENERALES.anuladoCod,
              pendienteCod = CONSTANTES_GENERALES.pendiente,
              nuevaProdCod = CONSTANTES_GENERALES.nuevaProdCod;
              

          $scope.setForm = function (form) {
            $scope.myForm = form;
          }
          
          $scope.setImporteForm = function (form) {
            $scope.myImporteForm = form;
          }          

          $scope.changeRenovacion =  function(){
            
            if($scope.model.cRenovacion === true){
              $scope.fApunteDisabled = true;
              $scope.model.fApunte = null;
            }else{
              $scope.fApunteDisabled = false;
            }
          }


          $scope.changeBusiness = function(){
           
            if($scope.model.cNegocio === codeNegocioInd ){              
              $scope.model.kSubpoliza = '0000000';
              $scope.model.kCertificado = '000000';
              $scope.subPolizaCertifDisabled = true;
            }else{
              $scope.subPolizaCertifDisabled = false;
            }
            
          }


          function isValidAltaApunteForm(){ 
            var msg = '',
                errors = [],
                formNameApunte = 'altaApunteForm',
                maxLengthImporte = CONSTANTES_GENERALES.DEFAULTOPTIONS.maxLengthImporte;

            alertaApuntesSrv.cleanFormAlert(); 
            errors = altaApuntesSrv.validateForm($scope.myForm);// validate required , maxlength
            var apunteForm = $scope.myForm;

            if(comunesSrv.isNullEmpty($scope.model.fApunte) && !$scope.model.cRenovacion){                  
              msg = comunesSrv.msgFieldMandatory('altaApunte.fApunte');
              errors.push({description: msg, form: formNameApunte, formControl: 'fApunte' });
            }

            if( new Date($scope.model.fFechaDesde) > new Date($scope.model.fFechaHasta)){
              msg = $filter('translate')('mensaje.fEfectoMenorMayor');              
              errors.push({description: msg, form: formNameApunte, formControl: 'fFechaDesde' });                
            }

            if(comunesSrv.isNullEmpty($scope.model.cOrigen) || $scope.model.cOrigen.toString() === '0'){              
              msg = comunesSrv.msgFieldMandatory('altaApunte.cOrigen');
              errors.push({description: msg, form: formNameApunte, formControl: 'cOrigen' });
            }

            if(comunesSrv.isNullEmpty($scope.model.cNegocio) || $scope.model.cNegocio.toString() === '0'){              
              msg = comunesSrv.msgFieldMandatory('altaApunte.cNegocio');
              errors.push({description: msg, form: formNameApunte, formControl: 'cNegocio' });
            }            
            
            if(comunesSrv.isNullEmpty($scope.model.cOperacion) || $scope.model.cOperacion.toString() === '0'){                            
              msg = comunesSrv.msgFieldMandatory('altaApunte.cOperacion');
              errors.push({description: msg, form: formNameApunte, formControl: 'cOperacion' });
            }              

            if(comunesSrv.isNullEmpty($scope.model.cTipoSoporte) || $scope.model.cTipoSoporte.toString() === '0'){                            
              msg = comunesSrv.msgFieldMandatory('altaApunte.cTipoSoporte');
              errors.push({description: msg, form: formNameApunte, formControl: 'cTipoSoporte' });
            }
            

            // validate cartera datas
            var fechaSuple = comunesSrv.ddmmyyyToDate($scope.model.fEfectoSuplemento);  
            if($scope.model.fEfectoSuplemento !== undefined && new Date($scope.model.fFechaDesde) < fechaSuple){
              msg = $filter('translate')('mensaje.fDesdefSuplemento') +" "+ $scope.model.fEfectoSuplemento;              
              errors.push({description: msg, form: formNameApunte, formControl: 'fFechaDesde' });                
            } 

            var fechaRenovacion = comunesSrv.ddmmyyyToDate($scope.model.fFechaRenovacion);            
            if($scope.model.fFechaRenovacion !== undefined && new Date($scope.model.fFechaHasta) > fechaRenovacion){
              msg = $filter('translate')('mensaje.fHastafRenovacion') +" "+ $scope.model.fFechaRenovacion;              
              errors.push({description: msg, form: formNameApunte, formControl: 'fFechaHasta' });                
            }  
              
            return errors;
          }          
          
          function loadGeneralDatas(){
            
            LoadingSrv.set($filter('translate')('cargando'));
            Promise
              .all([
                tablasGeneralesSrv.loadStates(),
                tablasGeneralesSrv.loadOrigen(),                
                tablasGeneralesSrv.loadBusiness(),
                tablasGeneralesSrv.loadTipoApunte(),
                tablasGeneralesSrv.loadTipoOperacion(),                
                tablasGeneralesSrv.loadTipoSoporte()
              ])
              .then(function(result) {

                $scope.statusList = result[0]; // estados
                $scope.origenList = comunesSrv.addSelecioneOption(result[1]); // origen
                $scope.businessList = comunesSrv.addSelecioneOption(result[2]); // companies
                $scope.typeNoteList = result[3]; // type notes
                $scope.typeOperationList = comunesSrv.addSelecioneOption(result[4]); // type operations
                $scope.typeSupportList = comunesSrv.addSelecioneOption(result[5]); // type support
                
                if($scope.altaFormAction === createForm){
                  LoadingSrv.hide();
                }
                
              });
          }

          // negocio, operación , póliza, subpóliza/grupo y certificado, fecha de apunte
          $scope.validateCartera = function(){
            
            if(!comunesSrv.isNullEmpty($scope.model.cNegocio) && 
              $scope.model.cNegocio.toString() !== '0' &&              
              !comunesSrv.isNullEmpty($scope.model.kPoliza) && 
              !comunesSrv.isNullEmpty($scope.model.kSubpoliza) && 
              !comunesSrv.isNullEmpty($scope.model.kCertificado)){

                
                if(comunesSrv.isNullEmpty($scope.model.fApunte) && $scope.model.cRenovacion !== true){
                  return false;
                }

                var dataReqCartera = {  
                  "cIdioma": "",
                  "fApunte": comunesSrv.convertDate($scope.model.fApunte),
                  "cNegocio": $scope.model.cNegocio,
                  "poliza": {  
                      "kPoliza": $scope.model.kPoliza,
                      "kSubpoliza": $scope.model.kSubpoliza,
                      "kSuplemento": $scope.model.kSuplemento,
                      "kCertificado": $scope.model.kCertificado                      
                  }
                }
                
                if(!_.isEqual(dataReqCartera, $scope.requestCartera)){

                  LoadingSrv.set($filter('translate')('cargando'));

                  carteraSrv.getConsultaPolizaCartera(dataReqCartera).then(function(result){

                    $scope.requestCartera = dataReqCartera; // to validate
                    // datas for tables
                    $scope.listaGarantias = result.poliza.listaGarantias;
                    $scope.listaProductores = result.poliza.listaProductores;
                    // extra datas cartera
                    $scope.model.fFechaRenovacion = result.poliza.fFechaRenovacion
                    $scope.model.fEfectoSuplemento = result.poliza.fEfectoSuplemento
                    $scope.model.cCompania = result.poliza.cCompania

                    LoadingSrv.hide();
                  });
                }
            }
          }          

          $scope.resetListGuarantAndProd = function(){            
            $scope.listaGarantias = [];
            $scope.listaProductores = [];            

            $scope.enableWarrantyButtons = false;
            $scope.enableProdButtons = false;
          }
          
          $scope.openModalValidation = function(){

            var errorsForm = isValidAltaApunteForm(), // validate alta apunte form            
                errorsImportes = altaApuntesSrv.validateBeforeCalculating(), // first at all we execute calcular apunte
                errors = errorsForm.concat(errorsImportes);  

            if(errors.length > 0){
              
              alertaApuntesSrv.addAlerts(errors);              
              $location.hash('end-main-container');                
              $anchorScroll();
            }else{ 
              $scope.$broadcast('executeCalcularApunte', { callbackFn: openModalAltaConfirm});                          
            }
          } 
          
          function openModalAltaConfirm(){

            // prepare info to show in the modal 
            var apunteModel = comunesSrv.cloneJson($scope.model),                    
                info = altaApunteInfo(apunteModel),
                dataAltaApunte = prepareDataAltaApunte(apunteModel),                    
                errors = altaApuntesSrv.validateImporteSign();
                        
            if(errors.length > 0){

              alertaApuntesSrv.addAlerts(errors);              
              $location.hash('end-main-container');                
              $anchorScroll();
            }else{              
              modalConfirmacionSrv.confirmAltaApuntes(info, dataAltaApunte, $scope.altaFormAction); // show modal
            }
            
          }

          function prepareDataAltaApunte(scopeModel){
            
            var datosApunte = scopeModel,
                fFechaDesde = comunesSrv.convertDate(scopeModel.fFechaDesde),
                fFechaHasta = comunesSrv.convertDate(scopeModel.fFechaHasta),
                fApunte = comunesSrv.convertDate(scopeModel.fApunte);

            var warrantyData = comunesSrv.getGridDatas("idGridGuarantee"),
                producerData = comunesSrv.getGridDatas("idGridProductor");                

            delete datosApunte.fFechaDesde;
            delete datosApunte.fFechaHasta;
            delete datosApunte.fApunte;            

            var extraData = {
              'kVersion': '001',
              'gCausa': '',
              'cAdhVol': false,
              'fFechaDesde': fFechaDesde,
              'fFechaHasta': fFechaHasta,
              'fApunte': fApunte
            }            

            return  {
              "cIdioma":"ES",
              "apunte":{  
                "datosApunte": Object.assign({}, datosApunte, extraData),
                "listaGarantias": warrantyData.datas,
                "listaProductores": producerData.datas
              }
            }
          }

          function altaApunteInfo(scopeModel){

            var tipoOperacioin = '';
            if(scopeModel.cOperacion === suplementoCod){
              tipoOperacioin = $filter('translate')('suplemento');
            }else if(scopeModel.cOperacion === nuevaProdCod){
              tipoOperacioin = $filter('translate')('nuevaProduccion');
            }else if(scopeModel.cOperacion === renovacionCod){
              tipoOperacioin = $filter('translate')('renovacion');
            }

            var estado = '',
                cEstado = scopeModel.cEstado;


            if(cEstado === pendienteCod && scopeModel.cSituacion === deRenovacionCod){
              estado = $filter('translate')('pendienteRevisar');
            }else if(cEstado === emitidoCod){
              estado = $filter('translate')('emitido');
            }else if(cEstado === anuladoCod){
              estado = $filter('translate')('anulado');
            }                        

            var info1 = $filter('translate')('altaApunte.importeTotalEs') + " " +  scopeModel.iTotalPrimaTotal + '€',
                info2 = $filter('translate')('altaApunte.apunteA') + " " + tipoOperacioin,
                info3 = $filter('translate')('altaApunte.apunteGrabarComo') + " " + estado,                
                info = [];
                
                // show fecha apunte if checkbox renovacion is checked
                if(!comunesSrv.isNullEmpty(scopeModel.cRenovacion) && scopeModel.cRenovacion.toString() === 'true'){
                  info2 = info2 +'. '+ $filter('translate')('fApunteEs') + scopeModel.fFechaRenovacion
                }

                info.push({'info': info1});
                info.push({'info': info2});
                info.push({'info': info3});                
                
            return info;
          }

          $scope.cancelAltaApunte = function(){
            
            var message = $filter('translate')('alertaConfirmacion.cancelarOperacion');
            modalConfirmacionSrv.alertConfirm(message, function(){
              $state.go('busquedaApuntes');
            })

          }

          $rootScope.$on('enableGridProdButtons', function(event , enable) {              
            $scope.enableProdButtons = enable;
          });   
          
          $rootScope.$on('enableGridWarrantyButtons', function(event , enable) {              
            $scope.enableWarrantyButtons = enable;
          }); 

          $scope.blurPagPoliza = function(){

            $scope.model.kPoliza = $scope.model.nPagPoliza;             
            $scope.validateCartera();
          }
          
          function initCreateFormModel(){

            console.log('create .............................');
            $scope.model = {
              // datos apunte
              fApunte: null,
              cEstado: 'PE',
              cRenovacion: null,
              fFechaDesde: null,
              fFechaHasta: null,
              cOrigen: '0',
              cNegocio: 'CO',
              cTipoApunte: 'MA',
              gObservaciones: '',
  
              // pagador
              nPagPoliza: null,
              nPagSubpoliza: null,
              nPagCertificado: null,
  
  
              // operacion
              kPoliza: null,
              kSubpoliza: null,
              kSuplemento: null,
              kCertificado: null,
              cOperacion: '0',
  
              // Datos de emisión
              nDiasDiferemiento: null,
              cTipoSoporte: '0',
              cCobroAutomatico: null,
  
              // Importes totalizados            
              iReajuste: '0',
              iAnticipo: '0',
              iRecobro: '0',
              iTotalPrimaNeta: null,
              iTotalPrimaTotal: null,
              iTotalDescuento: null,
              iTotalRecargo: null,
              iTotalRECCS: null,
              iTotalLEACS: null,
              iTotalImpuesto: null  
            }

            // disabled datos apunte
            $scope.fApunteDisabled = false;
            $scope.cEstadoDisabled = true;
            $scope.cNegocioDisabled = false;
            $scope.cOrigenDisabled = false; 
            $scope.cRenovacionDisabled = false; 
            
            
            // disabled datos del pagador
            $scope.nPagPolizaDisabled = false;            
            $scope.nPagSubpolizaDisabled = false;            
            $scope.nPagCertificadoDisabled = false;

            // disabled datos operacion
            $scope.kPolizaDisabled = true;            
            $scope.subPolizaCertifDisabled = false;            
            $scope.kSuplementoDisabled = false; 
            $scope.cOperacionDisabled = false; 

          }

          function initUpdateFormModel(dataSession, datas){

            console.log(datas)
            
            
            console.log('update .............................');            
            var dSelected = datas.datosApunte;
            //var dSelected = dataSession.session.dataApuntesSelected[0];

            $scope.model = {
              // datos apunte
              fApunte: dSelected.fApunte,
              cEstado: dSelected.cEstado,
              cRenovacion: null,
              fFechaDesde: dSelected.fFechaDesde,
              fFechaHasta: dSelected.fFechaHasta,
              cOrigen: dSelected.cOrigen,
              cNegocio: dSelected.cNegocio,
              cTipoApunte: dSelected.cTipoApunte,
              gObservaciones: dSelected.gObservaciones,
  
              // pagador
              nPagPoliza: dSelected.nPagPoliza,
              nPagSubpoliza: dSelected.nPagSubpoliza,
              nPagCertificado: dSelected.nPagCertificado,
  
  
              // operacion
              kPoliza: dSelected.kPoliza,
              kSubpoliza: dSelected.kSubpoliza,
              kSuplemento: dSelected.kSuplemento,
              kCertificado: dSelected.kCertificado,
              cOperacion: dSelected.cOperacion,
  
              // Datos de emisión
              nDiasDiferemiento: dSelected.nDiasDiferemiento,
              cTipoSoporte: dSelected.cTipoSoporte,
              cCobroAutomatico: dSelected.cCobroAutomatico,
  
              // Importes totalizados            
              iReajuste: dSelected.iReajuste,
              iAnticipo: dSelected.iAnticipo,
              iRecobro: dSelected.iRecobro,
              iTotalPrimaNeta: dSelected.iTotalPrimaNeta,
              iTotalPrimaTotal: dSelected.iTotalPrimaTotal,
              iTotalDescuento: dSelected.iTotalDescuento,
              iTotalRecargo: dSelected.iTotalRecargo,
              iTotalRECCS: dSelected.iTotalRECCS,
              iTotalLEACS: dSelected.iTotalLEACS,
              iTotalImpuesto: dSelected.iTotalImpuesto  
            }             

            // disabled datos del apunte 
            $scope.fApunteDisabled = false;
            $scope.cEstadoDisabled = true;             
            $scope.cNegocioDisabled = true;             
            $scope.cOrigenDisabled = true;  // en observacion campo sin dato 
            $scope.cRenovacionDisabled = true;        

            // disabled datos del pagador
            $scope.nPagPolizaDisabled = true;            
            $scope.nPagSubpolizaDisabled = true;            
            $scope.nPagCertificadoDisabled = true;
            
            // disabled datos operacion
            $scope.kPolizaDisabled = true;            
            $scope.subPolizaCertifDisabled = true;            
            $scope.kSuplementoDisabled = true; 
            $scope.cOperacionDisabled = true;  // en observacion campo sin dato              

            var warrantyOmitKey = ['iPrimaTotal'],
                producerOmitKey = ['iImporte'];
            
            $scope.listaGarantias = comunesSrv.jsonToCurrencyFormat(datas.listaGarantias, warrantyOmitKey);
            $scope.listaProductores = comunesSrv.jsonToCurrencyFormat(datas.listaProductores, producerOmitKey);            
          } 
          
          function init(){
            
            // clean alert message
            alertaApuntesSrv.cleanFormAlert();
            $scope.requestCartera = {};

            // list for dropdown
            $scope.statusList = [];
            $scope.origenList = [];
            $scope.businessList = [];
            $scope.typeNoteList = [];
            $scope.typeOperationList = [];
            $scope.typeSupportList = [];

            // datas for GRIDs
            $scope.listaGarantias = []; 
            $scope.listaProductores = [];   

            // field options                    
            $scope.enableProdButtons = false;
            $scope.enableWarrantyButtons = false;   
            
            
            // load form model edit/create
            var dataSessionApunte = navegacionSrv.get('altaApunte');
            if(dataSessionApunte === null || dataSessionApunte.session.actionApunte === 'createApunte'){

              $scope.altaFormAction = 'createForm';
              $scope.title = $filter('translate')('altaApunte.altaApunteTitle');
              initCreateFormModel();                            
            }else if(dataSessionApunte.session.actionApunte === 'editApunte') {

              $scope.title = $filter('translate')('altaApunte.modificarApunteTitle');
              $scope.altaFormAction = 'updateForm';

              prepareDetalleApunte(dataSessionApunte);
              
            }else if(dataSessionApunte.session.actionApunte === 'copyApunte') {

              $scope.title = $filter('translate')('altaApunte.copiarApunteTitle');
              $scope.altaFormAction = 'copyForm';

              prepareDetalleApunte(dataSessionApunte)
            }

            
            // load general datas
            loadGeneralDatas();   

          }

          function prepareDetalleApunte(dataSessionApunte){
          

            // call services to fill edit apunte form 
            var data = dataDetalleApunte(dataSessionApunte);
            apuntesSrv.detalleApunte(data).then(function(result){
              initUpdateFormModel(dataSessionApunte, result.apunte);              
              LoadingSrv.hide();              
            });
          }

          function dataDetalleApunte(dataSessionApunte){
            var dataFilter = dataSessionApunte.session.dataFilter,
                dataApuntesSelected = dataSessionApunte.session.dataApuntesSelected[0];

            return  {
              "cIdioma":"ES",
              "apunte":{  
                 "kPoliza": dataFilter.kPoliza,
                 "kSubpoliza": dataFilter.kSubpoliza,
                 "kSuplemento": dataFilter.kSuplemento,
                 "kCertificado": dataFilter.kCertificado,
                 "fApunte":dataApuntesSelected.fApunte,
                 "kVersion":"001"
              }                
            }            
          }

          init();

        }]);
})();
