/**
 * Created by Fred on 09/10/2017.
 * validate element when the user is typing
 */

(function () {
  'use strict';
  /*global appModule */
  (appModule.lazy || appModule)
      .directive('validInput', ['$window', function ($window) {

        var NUMBER_REGEXP = /^[0-9]+$/;

         return{
            require: 'ngModel',
            restrict:'A',
            link: function(scope, elem, attrs, modelCtrl) {

              var validAttr = scope.$eval(attrs.validInput),
                  maxlength = validAttr.maxlength,
                  type = validAttr.type;

              function validateMaxlength(){
                angular.element(elem).on("input", function(e) {
                  
                  var str = this.value;  
                  if(str.length <= maxlength){
                    return true;
                  }else{
                    str = str.substring(0, maxlength);
                    elem.val(str)
                  }  
                });
              }
        
              function validateNumber(){

                // convert to type text
                elem.attr("type", "text");
                
                modelCtrl.$parsers.push(function (inputValue) {
                  // this next if is necessary for when using ng-required on your input. 
                  // In such cases, when a letter is typed first, this parser will be called
                  // again, and the 2nd time, the value will be undefined                  

                  if (inputValue === undefined) return '' 
                  var transformedInput = inputValue.replace(/[^0-9]/g, ''); 
                  if (transformedInput!==inputValue) {
                      modelCtrl.$setViewValue(transformedInput);
                      modelCtrl.$render();
                  }         
        
                  return transformedInput;         
                });
              }


              // validate number
              if(type !== undefined && type === 'number'){
                validateNumber();
              }

              // validate maxlength
              if(maxlength !== undefined){
                validateMaxlength();
              }

            }
         }
      }]);

})();

