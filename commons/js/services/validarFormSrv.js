/**
 * Created by Fred on 9/24/2017.
 */
(function () {
    'use strict';
    /* global appModule */
    (appModule.lazy || appModule)
        .factory('validarFormSrv', ['HttpSrv', '$rootScope', 'comunesSrv', 
        'CONSTANTES_GENERALES',
        function ( HttpSrv, $rootScope, comunesSrv, CONSTANTES_GENERALES) {
          
          var lengthCurrency = CONSTANTES_GENERALES.DEFAULTOPTIONS.maxLengthImporte;

          // receives array of names input to validate field input
          function validateInputs(arrayInput, multiLanguage, formName){

            var errors = [];
            arrayInput.forEach(function(inputName) {              

              var item = angular.element('#' + inputName),
                  inputValue = angular.element('#' + inputName).val(),
                  options = $rootScope.$eval(item.attr("validate-field")),
                  msg = '';

              if(options !== undefined){

                var multilangCode = multiLanguage + '.' + inputName;               

                // validate required 
                if(options.required.toString() === 'true' && comunesSrv.isNullEmpty(inputValue)){                  
                    msg = comunesSrv.msgFieldMandatory(multilangCode);            
                    errors.push({description: msg, form: formName, formControl: inputName });
                }  
                
                // the currency must have 13 digits and 2 decimals 
                var inputNameDecimal = comunesSrv.parseCommaToDecimal(inputValue)
                if(options.type === 'currency' && comunesSrv.countIntegerNumber(inputNameDecimal) > lengthCurrency){                               
                  msg = comunesSrv.msgMaxLengthImporte(multilangCode);            
                  errors.push({description: msg, form: formName, formControl: inputName });
                }           
              }   

            });

            return errors;
          }
 

          return {
            validateInputs: validateInputs
          }         

        }]);

})();
