/**
 * Created by Fred on 9/24/2017.
 */
(function () {
    'use strict';
    /* global appModule */
    (appModule.lazy || appModule)
        .factory('permisosSrv', ['HttpSrv', function ( HttpSrv) {

          var rowsSelected = 0;
          var rowData = {}
          
          function setUserPermissions(userInfo, selRowIds, selectedRowData){

            rowsSelected = selRowIds;
            rowData = selectedRowData;

            var userPermissions = setDefaultPermissions();

            angular.forEach(userInfo.funcionalidades, function(element, key){               
              
              if(element.toString() === '0S002'){
                userPermissions = setAdministrativeProfile();
              }

              if(element.toString() === '0S001'){
                userPermissions = consultationProfile(userPermissions);
              }

              if(element.toString() === '0S005'){
                userPermissions = middleAdministrativeProfile(userPermissions);
              }
            });
            
            return userPermissions;
          }

          // code 0S001
          function consultationProfile(userPermissions){
            //var userPermissions = setDefaultPermissions();
            userPermissions.menuConsultaApunte = true;
            userPermissions.botonDetalleApunte = true; //always enabled
            return userPermissions;
          }

          // code 0S005
          function middleAdministrativeProfile(userPermissions){
            //var userPermissions = setDefaultPermissions();
            userPermissions.botonEmitirRecibo = true;
            userPermissions.botonDetalleApunte = true; //always enabled
            return userPermissions;
          }           
          
          function setDefaultPermissions(){
            var userPermissions = {
              botonAltaApunte: false,
              botonModifApunte: false,
              botonCopiarApunte: false,
              botonAnularApunte: false,
              botonCambioFrac: false,
              botonEmitirRecibo: false,
              menuConsultaApunte: false,
              botonDetalleApunte: false 
            };              

            return userPermissions;
          }

          // code 0S002
          function setAdministrativeProfile(){
           
            var userPermissions = {
              botonAltaApunte: true,
              botonModifApunte: conditionEditBtn(),
              botonCopiarApunte: conditionCopyBtn(),
              botonAnularApunte: true,
              botonCambioFrac: true,

              botonEmitirRecibo: false,
              menuConsultaApunte: false,
              botonDetalleApunte: true // always enabled                
            };

            return userPermissions;
          }

          function conditionEditBtn(){

            if(rowData !== null && rowData.cEstado === "Pendiente" && oneRowSelected() === true){
              return true;
            }else{
              return false;
            }
          }

          function conditionCopyBtn(){
            
            if(rowData !== null && oneRowSelected() === true && rowData.cEstado === "Anulado"){
              return true;
            }else{
              return false;
            }            
          }
          
          function oneRowSelected(){

            if(rowsSelected.length === 1){
              return true;
            }else{
              return false;
            }             
          }

          return {
            setUserPermissions: setUserPermissions,
            setDefaultPermissions: setDefaultPermissions,
            setAdministrativeProfile: setAdministrativeProfile,
            middleAdministrativeProfile: middleAdministrativeProfile,
            consultationProfile: consultationProfile
          };         

        }]);

})();
