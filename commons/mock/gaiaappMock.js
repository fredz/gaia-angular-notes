/*jslint node: true */
'use strict';

var DELAY = 1000,
    THEME = "",
    TOKEN = "1234567890";

function json(res, data) {
    setTimeout(function() {
        res.set({
            "X-User-theme": THEME
        });
        res.cookie('csrfCookie', TOKEN);
        res.json(data);
    }, DELAY);
}

// Your mocks in here

exports.postRegisterEvent = {
    method: 'post',
    url: '/api/register-event',
    mock: function(req, res) {
        json(res, {
            "mock": "OK"
        });
    }
};

module.exports.session = {
    method: 'get',
    url: '/api/session',
    mock: function(req, res) {
        json(res, {
            data: 600000 //0.5
        });
    }
};

exports.getUserInfo = {
    method: 'get',
    url: '/api/userinfo',
    mock: function(req, res) {
        json(res, {
            "data": {
                "name": "World",
                "firstLastName": "",
                "secondLastName": "",
                "dateOfBirth": -622598400000,
                "clientEmail": "john.doe@AcmeCorp.com",
                "nif": "01234567-A",
                "applicationData": {
                    "language": "en"
                }
            }
        });
    }
};
