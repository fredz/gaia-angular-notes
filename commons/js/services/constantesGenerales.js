/* global appModule */
"use strict";
(appModule.lazy || appModule).constant("CONSTANTES_GENERALES", {
	"DEFAULTOPTIONS" : {
    "SELECCIONE" : "- Seleccione -",
    'maxLengthImporte': '13',
    'importeFormat': {
      decimalSeparator:",", 
      thousandsSeparator: ".", 
      decimalPlaces: 2,
      defaultValue: ''
    }    
  },
  'codeNegocioInd': 'IN',
  'busqPorOper': 'operacion',
  'pendiente': 'PE',
  'createForm': 'createForm',
  'suplementoCod': 'SU',
  'nuevaProdCod': 'NP',
  'renovacionCod': 'RN',
  'deRenovacionCod': 'DR',
  'emitidoCod': 'EM',
  'anuladoCod': 'AN'
});
