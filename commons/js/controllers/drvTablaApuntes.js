/**
 * Created by Fred on 9/21/2017.
 */
(function () {
    'use strict';
    /*global appModule */
    (appModule.lazy || appModule)
        .directive('drvTablaApuntes', ['$window', 'busquedaApuntesSrv', '$filter', 
        'comunesSrv', function ($window, busquedaApuntesSrv, $filter,
          comunesSrv) {
          
           return{
              restrict: 'AE',
              link: function(scope, elm, attrs) {                
              },                          
              templateUrl: '/commons/html/drvTablaApuntesView.html',
              scope: {                
                mvApuntes: '=', // model table 
                mvFiltro: '=',  // datas table
                mvModo: '=',    // mode without buttons or with buttons 
                mvUsuario: '=', // user obj
                mvBuscar: '='   // function to seach notes
              },
              controller: ['$scope', 'permisosSrv', '$window', 
              'modalConfirmacionSrv', 'cambioFraccionamientoSrv', 'navegacionSrv', 
              '$state', 'busquedaApuntesSrv', function($scope, permisosSrv, $window, 
                modalConfirmacionSrv, cambioFraccionamientoSrv, navegacionSrv,
                $state, busquedaApuntesSrv) {

                $scope.emitirRecibo = function(){
                  if($scope.userPermissions.botonEmitirRecibo){
                    $window.location.href = '/#/emisionRecibo';
                  }              
                }
                
                $scope.detalleApunte = function(){
                  if($scope.userPermissions.botonDetalleApunte){
                    $window.location.href = '/#/detalleApunte';
                  }
                } 
    
                $scope.anularApunte = function(){
                  if($scope.userPermissions.botonAnularApunte){
                    var message = $filter('translate')('alertaConfirmacion.alertaAnularApuntes');
                    modalConfirmacionSrv.alertConfirm(message);
                  }
                }             
                
                $scope.copiarApunte = function(){              
                  if($scope.userPermissions.botonCopiarApunte){
                    var action = 'copyApunte',
                    sessionData = prepareSessionData(action);

                    navegacionSrv.set('altaApunte', sessionData);                    
                    $state.go('edicionApunte');
                  }
                }             
                
                $scope.modificarApunte = function(){
                  if($scope.userPermissions.botonModifApunte){
                    
                    var action = 'editApunte',
                        sessionData = prepareSessionData(action);

                    navegacionSrv.set('altaApunte', sessionData);                    
                    $state.go('edicionApunte');
                  }              
                }

                function prepareSessionData(action){
                  var sessionData = {};

                  sessionData.dataFilter = JSON.parse(JSON.stringify($scope.mvFiltro));
                  sessionData.actionApunte = action;
                  sessionData.listApuntes = JSON.parse(JSON.stringify($scope.mvApuntes));                  
                  sessionData.dataApuntesSelected = JSON.parse(JSON.stringify($scope.noteTableModel));
                  sessionData.rowIdSelected = comunesSrv.getGridRowSelected('idGridNote');

                  return sessionData;                 
                }


                
                $scope.altaApunteForm = function(){

                  //if($scope.userPermissions.botonAltaApunte){ 
                  
                    var action = 'createApunte',
                        sessionData = prepareSessionData(action);                    

                    navegacionSrv.set('altaApunte', sessionData);
                    $state.go('edicionApunte');
                  //}              
                } 
                
                $scope.openFractionation = function(){
                  
                  if($scope.userPermissions.botonCambioFrac){
                    cambioFraccionamientoSrv.openFractionation();
                  }
                }                

                function onSelectRowEvent(rowid, stat, e){ 
                  
                  var myGrid = angular.element('#idGridNote'),
                      selectedRowData = null,
                      selRowIds = myGrid.jqGrid("getGridParam", "selarrrow");                  
              
                  if(selRowIds !== undefined && selRowIds.length > 0) {

                    if(selRowIds.length === 1){
                      selectedRowData = myGrid.getRowData(selRowIds[0]);
                    }

                    $scope.userPermissions = permisosSrv.setUserPermissions($scope.mvUsuario, selRowIds, selectedRowData);
                  }else{
                    $scope.userPermissions = permisosSrv.setDefaultPermissions();
                  } 
                }

                function getTableOptions(){

                  var tableOptions = busquedaApuntesSrv.getNoteTableOptions();
                  tableOptions.onSelectRow = onSelectRowEvent;
                  
                  return tableOptions;
                }


                function init(){
                  
                  // init permissions
                  $scope.userPermissions = permisosSrv.setDefaultPermissions();
                  
                  // init model note table 
                  $scope.noteTableOptions = getTableOptions();  
                  
                  $scope.noteTableModel = [];
                }

                init();
              }]
           }
        }]);

})();
