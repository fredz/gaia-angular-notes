/**
 * Created by Fred on 9/21/2017.
 */
(function () {
    'use strict';
    /*global appModule */
    (appModule.lazy || appModule)
        .directive('drvBusquedaApuntes', ['$window', 'busquedaApuntesSrv', 'LoadingSrv',
        'tablasGeneralesSrv', 'usuarioSrv', 'companiasSrv',
        'comunesSrv', '$anchorScroll', 
        '$location', 'alertaApuntesSrv',
        '$filter', 'apuntesSrv', 'navegacionSrv', 'CONSTANTES_GENERALES', function ($window, busquedaApuntesSrv, LoadingSrv, 
          tablasGeneralesSrv, usuarioSrv, companiasSrv,
          comunesSrv, $anchorScroll, 
          $location, alertaApuntesSrv, 
          $filter, apuntesSrv, navegacionSrv, CONSTANTES_GENERALES) {

            var busqOper = CONSTANTES_GENERALES.busqPorOper,
                codeNegocioInd = CONSTANTES_GENERALES.codeNegocioInd,
                pendiente = CONSTANTES_GENERALES.pendiente;
          
            return{
              restrict: 'AE',
              link: function(scope, elm, attrs) {
              },                          
              templateUrl: '/commons/html/drvBusquedaApuntesView.html',
              scope: {                  
                userInfo: '=mvUsuario', // user obj
                mvModo: '@'
              },              
              controller: ['$scope', function($scope) {
                         
                $scope.noteList = [];
                $scope.mvModoTabla = 'operacionCopiar';                                
    
                // init default values 
                function init(){

                  defaultDisableInputs();                  

                  // clean error message
                  alertaApuntesSrv.cleanFormAlert(); 

                  loadGeneralDatas();

                  var dataSessionApunte = navegacionSrv.get('altaApunte'); 
                  if(dataSessionApunte !== null){                    
                    // fill from session data  
                    fillSessionData(dataSessionApunte);
                  }else{
                    // default model
                    defaultValues();
                  }
                }

                function defaultDisableInputs(){
                  $scope.disabledSubpoliza = true;
                  $scope.disabledSuplemento = true;
                  $scope.disabledCertificate = true;
                  $scope.disabledSituacion = false;              
                }                

                function fillSessionData(dataSessionApunte){

                    var dataFilter = dataSessionApunte.session.dataFilter,
                        listApuntes = dataSessionApunte.session.listApuntes,
                        fApunteDesde = dataFilter.fApunteDesde,
                        fApunteHasta = dataFilter.fApunteHasta,
                        dataApuntesSelected = dataSessionApunte.session.dataApuntesSelected,
                        rowIdSelected = dataSessionApunte.session.rowIdSelected;                    

                    if(comunesSrv.isDate(fApunteDesde)){
                      dataFilter.fApunteDesde = new Date(fApunteDesde);
                    }

                    if(comunesSrv.isDate(fApunteHasta)){
                      dataFilter.fApunteHasta = new Date(fApunteHasta);
                    }

                    $scope.model = dataFilter;                    

                    // fire event
                    $scope.changeBusquedaPor();

                    // fire event 
                    if(listApuntes.length > 0){
                      $scope.searchNotes(function(){
                        // set row selected
                        comunesSrv.setGridSelectedRow('idGridNote', rowIdSelected);
                      });
                    }
                }
    
                function loadGeneralDatas(){
    
                  LoadingSrv.set($filter('translate')('cargando'));
                  Promise
                    .all([
                      tablasGeneralesSrv.loadStates(),
                      tablasGeneralesSrv.loadBusiness(),
                      tablasGeneralesSrv.loadSituations(),
                      companiasSrv.loadCompanies()
                      ])
                    .then(function(result) {
                      var states = result[0],
                          business = result[1],
                          situations = result[2],
                          companies = result[3].listadoCompanias;
    
                      // states
                      $scope.opcionesEstados = states;
                      
                      // business
                      $scope.businessOptions = comunesSrv.addSelecioneOption(business);                  
    
                      // situations                     
                      $scope.situationOptions = comunesSrv.addTodosOption(situations);  
                      
                      // companies                  
                      $scope.companiesOptions = comunesSrv.addDefaultCompany(companies);
    
                      LoadingSrv.hide();
                    });
                }


                function validateSearchNotes(){ 

                  var msg = '',
                      errors = [],
                      formName = 'noteSearchForm';                  
    
                  if( new Date($scope.model.fApunteDesde) > new Date($scope.model.fApunteHasta)){
    
                    msg = $filter('translate')('mensaje.msgFechaDesdeHasta');
                    errors.push({description: msg, form: formName, formControl: 'fApunteDesde' });                
                  }                  
                    
                  if(comunesSrv.isNullEmpty($scope.model.cCompania) || $scope.model.cCompania.toString() === '0' ){
                    msg = $filter('translate')('mensaje.companiaRequerido');
                    errors.push({description: msg, form: formName, formControl: 'cCompania' });
                  }
                  
                  
                  if(comunesSrv.isNullEmpty($scope.model.cNegocio) || $scope.model.cNegocio.toString() === '0'  ){
                    msg = $filter('translate')('mensaje.negocioRequerido');
                    errors.push({description: msg, form: formName, formControl: 'cNegocio' });
                  } 
                  
                  return errors;                 
                }

                $scope.searchNotes = function(myCallback){

                  var errors = validateSearchNotes();
                  alertaApuntesSrv.cleanFormAlert();                   
                  
                  if(errors.length > 0){

                    alertaApuntesSrv.addAlerts(errors); // show errors alert                    
                    $location.hash('end-main-container');                
                    $anchorScroll();                    

                  }else{

                    LoadingSrv.set($filter('translate')('cargando'));                                                         
                    var dataRequest = busquedaApuntesSrv.prepareBusqApunteRequest($scope.model);                     
      
                    apuntesSrv.getNotes(dataRequest).then(function(result){

                      $scope.noteList = result.listaApuntes; 
                      comunesSrv.resetSelectionGrid('idGridNote'); 
                      
                      // execute callback
                      if (myCallback && typeof(myCallback) === "function"){
                        myCallback();
                      }                      
                      
                      LoadingSrv.hide();
                    }); 
                  }

                }

                function defaultValues(){

                  $scope.model = {
                      cCompania: '10',
                      cNegocio: 'CO',
                      listaEstados:['PE', 'EM'],
                      cSituacion:"TO",
                      fApunteDesde: '',
                      fApunteHasta: '',
                      busquedaPor:'pagador',
                      kPoliza: null,
                      kCertificado: null,
                      kSubpoliza: null,
                      kSuplemento: null
                  };
  
                  $scope.noteList = [];
                  alertaApuntesSrv.cleanFormAlert();

                }
  
                $scope.clean = function(){
                  defaultValues();
                  defaultDisableInputs();
                };
              
                $scope.validatePoliza = function(){
                  
                  if($scope.model.kPoliza === null || $scope.model.kPoliza === ''){
                    
                    $scope.model.kSubpoliza = null;                                
                    $scope.model.kCertificado = null;
    
                    $scope.disabledSubpoliza = true;                
                    $scope.disabledCertificate = true;
                  }else{
    
                    if($scope.model.cNegocio !== codeNegocioInd){
                      $scope.disabledSubpoliza = false;
                    }                
                  }
                  
                }
                          
                $scope.validateSubPoliza = function(){
    
                  if($scope.model.kSubpoliza === null ){ 
    
                    $scope.model.kCertificado = null;
                    $scope.disabledCertificate = true;
    
                  }else{
                    if($scope.model.cNegocio !== codeNegocioInd){
                      $scope.disabledCertificate = false;
                    }
                  }
                  
                }      
    
                $scope.onChangeBusiness = function(){              
                  
                  if($scope.model.cNegocio === codeNegocioInd){
    
                    $scope.model.kSubpoliza = null;
                    $scope.model.kCertificado = null;
    
                    $scope.disabledSubpoliza = true;
                    $scope.disabledCertificate = true;
                  }
                }
  
                $scope.conditionsNotesForm = function(){
                  $scope.validatePoliza();
                  $scope.validateSubPoliza();              
                  $scope.onChangeBusiness();
    
                }
    
                $scope.changeBusquedaPor = function(){
                  
                  if($scope.model.busquedaPor === busqOper){
                    $scope.disabledSuplemento = false;
                  }else{
                    $scope.disabledSuplemento = true;
                    $scope.model.kSuplemento = '';
                  }
                }
  
                $scope.onChangeState = function(){
                  
                  var isDisabled = true;
                  angular.forEach($scope.model.listaEstados, function(state, key) {
                    if(state === pendiente){
                      isDisabled = false;
                    }
                  });
    
                  $scope.disabledSituacion = isDisabled;
                }

                init();

              }]              
           }
        }]);

})();
