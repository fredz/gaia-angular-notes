/**
 * Created by Fred on 9/27/2017.
 */
(function () {
  'use strict';
  /* global appModule */
  (appModule.lazy || appModule)
      .factory('busquedaApuntesSrv', ['$filter', 'comunesSrv', 'CONSTANTES_GENERALES', 
      function ($filter, comunesSrv, CONSTANTES_GENERALES) {

        var importeFormat = CONSTANTES_GENERALES.DEFAULTOPTIONS.importeFormat;

        function getNoteTableOptions(){
          return {
            'datatype': 'local',
            'multiselect': true,
            'height': 'auto',
            'colNames': [                  
                $filter('translate')('pagador'),
                $filter('translate')('icono'),
                $filter('translate')('operacion'),
                $filter('translate')('estado'),
                $filter('translate')('fApunte'),
                $filter('translate')('importe'),
                $filter('translate')('reajuste'),
                $filter('translate')('tipo')
            ],
            'colModel': [{
              'name': '',
              'index': '',
              'align': 'left', 
              'width': 190,                               
              'formatter': myPagadorformat,              
              'frozen': true
            },{
              'name': '',
              'index': '',
              'align': 'center',
              'width': 60 ,
              'formatter': myIconFormat,              
              'frozen': true
            },{
              'name': '',
              'index': '',
              'align': 'left',
              'width': 260,              
              'formatter': myOperacionFormat
            },{
              'name': 'cEstado',
              'index': 'cEstado',
              'align': 'left',              
              'width': 120,
              'formatter': myEstadoFormat
            },{
              'name': 'fApunte',
              'index': 'fApunte',
              'align': 'center',              
              'width': 120
            },{
              'name': 'iPrimaTotal',
              'index': 'iPrimaTotal',
              'width': 120,              
              'align': 'right',
              'formatter': function(cellvalue, options, rowObject){
                return comunesSrv.formatMoney(cellvalue, '2', '.', ',');
              }
            },{
              'name': 'iReajuste',
              'index': 'iReajuste',
              'width': 120,              
              'align': 'right',
              'formatter': function(cellvalue, options, rowObject){
                return comunesSrv.formatMoney(cellvalue, '2', '.', ',');
              }
            },{
              'name': 'cTipoApunte',
              'index': 'cTipoApunte',
              'align': 'left',              
              'width': 120,
              'formatter': myTipoApunteFormat
            }],            
            'shrinkToFit': false,
            'loadonce': true,
            'rownumbers': false,
            'cmTemplate': { sortable: false, 'fixed': true },
            'width': 700,
            //'scroll': true,
            'rowNum': 100                                   
          }
        }

        function myPagadorformat ( cellvalue, options, rowObject ){
          return "<span class='fixed-column-serach' >"+rowObject.kPoliza+'/'+rowObject.kSubpoliza+'/'+rowObject.kCertificado+"</span>";
        }

        function myIconFormat(cellvalue, options, rowObject){
          
          var icon = 'pendienteRenoR.png';
          
          if(rowObject.cEstado === 'PE' && rowObject.cSituacion === 'AR'){
            // estado pendiente y su situación es revisar
            icon = 'pendienteRevR.png';
          }else if(rowObject.cEstado === 'PE' && rowObject.cSituacion === 'DE'){
            // pendiente y su situación es emitir
            icon = 'pendienteEmitirR.png';
          }else if(rowObject.cEstado === 'PE' && rowObject.cSituacion === 'DR'){
            // estado pendiente y su situación es renovación
            icon = 'pendienteRenoR.png';
          }

          return '<img title="'+ getLiteralState(rowObject.cEstado) +'" src="commons/img/'+icon+'">';
        }

        function myOperacionFormat(cellvalue, options, rowObject){
          return rowObject.kPoliza+'/'+rowObject.kSubpoliza+'/'+rowObject.kSuplemento+'/'+rowObject.kCertificado;
        }

        function myEstadoFormat(cellvalue, options, rowObject){
          return getLiteralState(rowObject.cEstado);
        }

        function getLiteralState(state){
          
          var result = '';

          if(state === 'PE'){
            result = $filter('translate')('pendiente');
          }else if(state === 'EM'){
            result = $filter('translate')('emitido');
          }else if(state === 'AN'){
            result = $filter('translate')('anulado');
          }

          return result;          
        }

        function myTipoApunteFormat(cellvalue, options, rowObject){

          if(rowObject.cTipoApunte === 'CA'){
            return $filter('translate')('cartera');
          }else if(rowObject.cTipoApunte === 'MA'){
            return $filter('translate')('manual');
          }
        }

        function prepareBusqApunteRequest(dataModel){

          var model = JSON.parse(JSON.stringify(dataModel)),
              fApunteDesde = model.fApunteDesde,
              fApunteHasta = model.fApunteHasta;

          fApunteDesde = (fApunteDesde === '')?'':comunesSrv.convertDate(fApunteDesde);
          fApunteHasta = (fApunteHasta === '')?'':comunesSrv.convertDate(fApunteHasta);

          // prepare state list
          var stateList = [];
          model.listaEstados.forEach(function(value) {                
            stateList.push({cEstado: value});
          });
          
          var data = {
            "cIdioma":"ES",
            "filtro":{  
              "cCompania":model.cCompania,
              "cNegocio":model.cNegocio,
              "listaEstados": stateList,
              "cSituacion":model.cSituacion.COD_VALOR,
              "fApunteDesde": fApunteDesde,//comunesSrv.convertDate(model.fApunteDesde),
              "fApunteHasta": fApunteHasta, //comunesSrv.convertDate(model.fApunteHasta),
              "nPagPoliza":"0007000014",
              "nPagSubpoliza":"",
              "nPagCertificado":"",
              "kPoliza":model.kPoliza,
              "kSubpoliza":model.kSubpoliza,
              "kSuplemento":model.kSuplemento,
              "kCertificado":model.kCertificado
            },
            "paginación":{  
              "kPoliza":"",
              "kSubpoliza":"",
              "kSuplemento":"",
              "kCertificado":"",
              "kVersion":"",
              "fApunte":""
            }
          };
          
          return data;
        }
        
        function noteTableGridSet() {          
          var myGrid = angular.element('#idGridNote');
          myGrid.jqGrid('setFrozenColumns');

          return myGrid;
        }
        
        return {
          getNoteTableOptions: getNoteTableOptions,
          prepareBusqApunteRequest: prepareBusqApunteRequest,
          noteTableGridSet: noteTableGridSet
        }

      }]);

})();



