/**
 * Created by Fred on 9/22/2017.
 */
(function () {
    'use strict';

    /*global appModule */
    (appModule.lazy || appModule)
        .controller('EmisionReciboCtrl', ['$scope', 'apuntesSrv', '$window', function ($scope, apuntesSrv, $window) {

          $scope.back = function(){
            $window.history.back();
          }

        }]);
})();
