/**
 * Created by Fred on 9/10/2017.
 */
(function () {
    'use strict';
    /* global appModule */
    (appModule.lazy || appModule)
        .factory('usuarioSrv', ['rutaApuntesRecibo', 'HttpSrv', 'zeroConfigSrv', function (rutaApuntesRecibo, HttpSrv, zeroConfigSrv) {  
          
          var URLApuntesRecibo = '';

          function currentUser() {
            var url = '/api/loguedUser';  
            return zeroConfigSrv.getZeroConfig().then(function(data){
              
              URLApuntesRecibo = data.zeroConfig.URLApuntesRecibo;
              // url = URLApuntesRecibo + url;
              return HttpSrv.get(url);
            });            
          }          
          
          function userLife() {

            var url ='/api/objUser/user/cUsuario/info';

            return zeroConfigSrv.getZeroConfig().then(function(data){
              
              URLApuntesRecibo = data.zeroConfig.URLApuntesRecibo;
              // url = URLApuntesRecibo + url;
              return HttpSrv.get(url);
            });            
          }
          
          return {
            currentUser: currentUser,
            userLife: userLife
          };         

        }]);

})();
