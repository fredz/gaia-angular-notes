/**
 * Created by Fred on 10/10/2017.
 */
(function () {
    'use strict';

    function drvTabDatosProductorCtrl($scope, altaApuntesSrv, $rootScope, comunesSrv) {

      $scope.productorListOptions = altaApuntesSrv.getProductorListOptions();
      var lastSelection; // for grid producer data
      
      $scope.addProducer = function(){
        var myGrid = angular.element('#idGridProductor'),
            producerList = myGrid.getGridParam('data'),
            rowId = producerList.length + 1,
            mydata = {"kTipoIntervencion":"","pPorcentaje":"", "iImporte": ""};

        // save all rows mode editing
        comunesSrv.saveAllRowGrid("idGridProductor");             
        
        myGrid.jqGrid('addRowData', rowId, mydata);

        // enaable editable columns
        myGrid.jqGrid('setColProp', 'kTipoIntervencion', {editable:true});                                                         

        setTimeout(function(){ 
          myGrid.jqGrid('editRow',rowId );
        }, 250);        

      }

      $scope.editProducer = function(){       

        if($scope.enableProdButtons === true){
          var myGrid = angular.element('#idGridProductor'),
              selectedRowId = myGrid.jqGrid ('getGridParam', 'selrow');         


          if (selectedRowId) {

            // save all rows mode editing
            comunesSrv.saveAllRowGrid("idGridProductor");                           

            // disable editable columns
            var productorId = myGrid.jqGrid ('getCell', selectedRowId, 0);
            if(productorId.length > 0){
              myGrid.jqGrid('setColProp', 'kTipoIntervencion', {editable:false});
            }else{
              myGrid.jqGrid('setColProp', 'kTipoIntervencion', {editable:true});
            }

            myGrid.jqGrid('editRow', selectedRowId);            
          }
        }
      }

      $scope.deleteProducer = function(){

        if($scope.enableProdButtons === true){

          var myGrid = angular.element('#idGridProductor'),
              rowId = myGrid.jqGrid ('getGridParam', 'selrow');
     
          myGrid.jqGrid('delRowData', rowId);
          
          // disable actions button producers
          $rootScope.$emit('enableGridProdButtons', false);
        }
      }
      
    }

    /*global appModule */
    (appModule.lazy || appModule)
        .directive('drvTabDatosProductor', ['$window', function ($window) {
          
           return{
              restrict: 'AE',
              link: function(scope, elm, attrs) {
              },                          
              templateUrl: '/commons/html/drvTabDatosProductorView.html',
              controller: ['$scope', 'altaApuntesSrv', '$rootScope', 'comunesSrv', drvTabDatosProductorCtrl]
           }
        }]);

})();
