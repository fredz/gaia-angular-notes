/**
 * Created by Fred on 9/24/2017.
 */
(function () {
    'use strict';
    /* global appModule */
    (appModule.lazy || appModule)
        .factory('comunesSrv', ['CONSTANTES_GENERALES', '$filter', function ( CONSTANTES_GENERALES, $filter) {  

          var seleccioneOption = CONSTANTES_GENERALES.DEFAULTOPTIONS.SELECCIONE;

          function addSelecioneOption(list){
            list.unshift({COD_VALOR:'0', NOM_VALOR: seleccioneOption});                
            return list;
          }

          function addTodosOption(list){
            list.unshift({"COD_VALOR":"TO", "NOM_VALOR":"Todos"});            
            return list;                
          }  
          
          function addDefaultCompany(list){
            list.unshift({cCompania: '0', gDescripcion: seleccioneOption}) ;           
            return list;                
          } 
          
          // return format date dd/mm/yyyy
          function convertDate(inputFormat) {

            function pad(s) { return (s < 10) ? '0' + s : s; }
            var d = new Date(inputFormat);
            return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('/');                          
          }
          
          function isNullEmpty(str){
            var valid = false;

            if(str === '' || str === null || str === undefined){
              valid = true;
            }

            return valid;
          }

          function msgFieldMandatory(fieldCode){
            return $filter('translate')(fieldCode) + ': ' + $filter('translate')('mensaje.campoRequerido');
          }

          function msgFieldNumeric(fieldCode){
            return $filter('translate')(fieldCode) + ': ' + $filter('translate')('mensaje.campoNumerico');
          }
          
          function msgMaxLength(fieldCode, maxlength){
            return $filter('translate')(fieldCode) + ': ' + $filter('translate')('mensaje.maxLength') + ' ' + maxlength;
          }
          
          function msgMaxLengthImporte(fieldCode){
            return $filter('translate')(fieldCode) + ': ' + $filter('translate')('mensaje.maxLengthImporte');
          }             

          function isDate(input) {
            var status = false;
            if (!input || input.length <= 0) {
              status = false;
            } else {
              var result = new Date(input);
              if (result === 'Invalid Date') {
                status = false;
              } else {
                status = true;
              }
            }
            return status;
          }
          
          function countIntegerNumber(str){

            if(isNullEmpty(str)){
              return 0;
            }else{
              return (Math.ceil(Math.log(str + 1) / Math.LN10)) - 1;
            }
          }          

          function onlyNumbers(){
            return /^[0-9]+$/;
          }

          function parseCommaToDecimal(n) {            
            return n.replace(/\./g, '').replace(',', '.');            
          }

          function formatMoney(num, decPlaces, thouSeparator, decSeparator){

            if(isNullEmpty(num)){
              return '';
            }else{
              num = num.replace(/\./g, '').replace(',', '.');
              decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 0 : decPlaces;
              decSeparator = decSeparator === undefined ? "," : decSeparator;
              thouSeparator = thouSeparator === undefined ? "." : thouSeparator;
  
              var sign = num < 0 ? "-" : "",
                  i = parseInt(num = Math.abs(+num || 0).toFixed(decPlaces), 10) + "",
                  j = (j = i.length) > 3 ? j % 3 : 0;
  
              return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(num - i).toFixed(decPlaces).slice(2) : "");
            }
          }

          function getGridRowSelected(gridId){
            
            var myGrid = angular.element('#' + gridId),
            selRowIds = myGrid.jqGrid("getGridParam", "selarrrow");            
            return selRowIds;
          }

          function setGridSelectedRow(gridId, rowArray){
            var i, 
                count, 
                myGrid = angular.element("#" + gridId);

            setTimeout(function(){ 
              for (i = 0, count = rowArray.length; i < count; i += 1) {
                myGrid.jqGrid("setSelection", rowArray[i]);
              }     
            }, 500);
          }

          function getGridDatas(gridId){
            
            var myGrid = angular.element('#' + gridId),
                datas = myGrid.getGridParam('data'),
                colNames = myGrid.jqGrid('getGridParam','colNames');
        
            return {
              'datas': datas,
              'colNames': colNames
            };
          }

          function cloneJson(obj){
            return JSON.parse(JSON.stringify(obj))
          }
          
          function isObjEmptyValues(obj){
            var hasEmptyValues = false;
            for (var key in obj) {
              if (isNullEmpty(obj[key])) {
                hasEmptyValues = true;
              }
            }
            return hasEmptyValues;
          }

          function convertObjToArray(obj){
            return Object.keys(obj).map(function(x) { return obj[x]; })
          }

          function resetSelectionGrid(gridId){
            var myGrid = angular.element('#' + gridId);
            myGrid.jqGrid('resetSelection');            
          }

          function saveAllRowGrid(gridId){
            
            var myGrid = angular.element('#' + gridId),
                ids = myGrid.jqGrid('getDataIDs'), 
                i, 
                length = ids.length;

            for (i = 0; i < length; i++) {
                myGrid.jqGrid('saveRow', ids[i], true);
            }          
          }
          
          // convert dd/mm/yyyy to mm/dd/yyyyy
          function ddmmyyyToDate(dateString){

            if(dateString === undefined){
              return new Date(null);
            }else{
              var dateParts = dateString.split("/");
              return new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);                          
            }
          }

          function hasKey(array, key){

            var existKey = false

            for (var k in array) {
              if(array[k] === key){
                existKey = true;
              }
            }

            return existKey;
          }

          function jsonToCurrencyFormat(jsonArray, arrayOmitKey){
            var i, key, keys;           

            jsonArray.forEach(function(element) {
              keys = Object.keys(element);
              for(i=0; i<keys.length; i++){
                key = keys[i];

                if(!hasKey(arrayOmitKey, key)){
                  element[key] = parseCommaToDecimal(element[key])
                }
              }     
            });     
                               
            return jsonArray;
          }


          return {
            addSelecioneOption: addSelecioneOption,
            addTodosOption: addTodosOption,
            addDefaultCompany: addDefaultCompany,
            convertDate: convertDate,
            isNullEmpty: isNullEmpty,
            msgFieldMandatory: msgFieldMandatory,
            isDate: isDate,
            msgFieldNumeric: msgFieldNumeric,
            msgMaxLength: msgMaxLength,
            countIntegerNumber: countIntegerNumber,
            msgMaxLengthImporte: msgMaxLengthImporte,
            onlyNumbers : onlyNumbers,
            parseCommaToDecimal: parseCommaToDecimal,
            formatMoney: formatMoney,
            getGridRowSelected: getGridRowSelected,
            setGridSelectedRow: setGridSelectedRow,
            getGridDatas: getGridDatas,
            isObjEmptyValues: isObjEmptyValues,
            cloneJson: cloneJson,
            convertObjToArray: convertObjToArray,
            resetSelectionGrid: resetSelectionGrid,
            saveAllRowGrid: saveAllRowGrid,
            ddmmyyyToDate: ddmmyyyToDate,
            jsonToCurrencyFormat: jsonToCurrencyFormat
          };         

        }]);

})();
