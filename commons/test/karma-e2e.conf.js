/*global ANGULAR_SCENARIO, ANGULAR_SCENARIO_ADAPTER, LOG_DISABLE, LOG_ERROR, LOG_WARN, LOG_INFO, LOG_DEBUG */
(function (global) {
    'use strict';
    // Will be used to resolve files and exclude
    global.basePath = 'C:\GAIA_Front\workspace\src';

    // List of files/patterns to load in the browser
    global.files = [
        ANGULAR_SCENARIO, // Required for testing
        ANGULAR_SCENARIO_ADAPTER, // Required for testing
        'commons/test/e2e/scenarios.js' // Tests
    ];

    // Enable/disable watching file and executing tests whenever any file changes
    global.autoWatch = false;

    // Start these browsers. Currently available: Chrome || ChromeCanary || Firefox || Opera || Safari || PhantomJS
    global.browsers = ['PhantomJS'];

    // Test results reporter to use. Possible values: dots || progress
    global.reporter = 'progress';

    // Web server port
    global.port = 55504;

    // Cli runner port
    global.runnerPort = 55504;

    // Enable/disable colors in the output (reporters and logs)
    global.colors = true;

    // Level of logging. Possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
    global.logLevel = LOG_DEBUG;

    // Continuous Integration mode. If true, it capture browsers, run tests and exit
    global.singleRun = true;

    // This is the root url for the testacular proxy. IMPORTANT!!
    global.urlRoot = '/__testacular/';

    // In here you configure the proxy.
    global.proxies = {
        '/': 'http://localhost:55504/'
    };
}(this));