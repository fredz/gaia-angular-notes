/**
 * Created by Fred on 9/24/2017.
 */
(function () {
  'use strict';
  /* global appModule */
  (appModule.lazy || appModule)
      .factory('altaApuntesSrv', ['$filter', '$rootScope', '$modal',
      'HttpSrv', 'comunesSrv', 'CONSTANTES_GENERALES', 'validarFormSrv', function ( $filter, $rootScope, $modal, 
        HttpSrv, comunesSrv, CONSTANTES_GENERALES, validarFormSrv) {          

        var importeFormat = CONSTANTES_GENERALES.DEFAULTOPTIONS.importeFormat,
            maxLengthImporte = CONSTANTES_GENERALES.DEFAULTOPTIONS.maxLengthImporte;

        var msgRequired = $filter('translate')('mensaje.campoRequerido'),
            msgMaxlength = $filter('translate')('mensaje.maxLength'),
            msgNumber = $filter('translate')('mensaje.campoNumerico');

        function getGuaranteeListOptions(){
          return {
            'datatype': 'local',
            'height': 'auto',
            'editurl': 'clientArray',
            'colNames': [                  
                $filter('translate')('altaApunte.identificador'),
                $filter('translate')('altaApunte.total'),
                $filter('translate')('altaApunte.prima'),
                $filter('translate')('altaApunte.descuento'),
                $filter('translate')('altaApunte.recargoPagoFraccionado'),
                $filter('translate')('altaApunte.RECCS'),
                $filter('translate')('altaApunte.recargoRECCS'),
                $filter('translate')('altaApunte.LEACS'),
                $filter('translate')('altaApunte.recargoLEACS'),
                $filter('translate')('altaApunte.impuestos')
            ],
            'colModel': [{
              'name': 'kGarantia',
              'index': 'kGarantia',
              'align': 'right', 
              'width': 100,                                               
              'frozen': true
            },{
              'name': 'iPrimaTotal',
              'index': 'iPrimaTotal',
              'align': 'right',
              'width': 160 ,
              'frozen': true,
              'formatter': function(cellvalue, options, rowObject){
                return comunesSrv.formatMoney(cellvalue, '2', '.', ',');
              }              
            },{
              'name': 'iPrimaNeta',
              'index': 'iPrimaNeta',
              'align': 'right',
              'width': 130 ,
              'editable': true,
              formatter:'currency',
              formatoptions: importeFormat,
              editoptions: {
                type: "number"
              }
            },{
              'name': 'iDescuento',
              'index': 'iDescuento',
              'align': 'right',              
              'width': 130,
              'editable': true,
              formatter:'currency',
              formatoptions: importeFormat,
              editoptions: {
                type: "number"
              }
            },{
              'name': 'iRecargoPagoFraccionado',
              'index': 'iRecargoPagoFraccionado',
              'align': 'right',              
              'width': 160,
              'editable': true,
              formatter:'currency',
              formatoptions: importeFormat,
              editoptions: {
                type: "number"
              }
            },{
              'name': 'iRECCS',
              'index': 'iRECCS',
              'width': 130,              
              'align': 'right',
              'editable': true,
              formatter:'currency',
              formatoptions: importeFormat,
              editoptions: {
                type: "number"
              }
            },{
              'name': 'iRecargoRECCS',
              'index': 'iRecargoRECCS',
              'align': 'right',              
              'width': 130,
              'editable': true,
              formatter:'currency',
              formatoptions: importeFormat,
              editoptions: {
                type: "number"
              }
            },{
              'name': 'iLEACS',
              'index': 'iLEACS',
              'width': 130,              
              'align': 'right',
              'editable': true,
              formatter:'currency',
              formatoptions: importeFormat,
              editoptions: {
                type: "number"
              }
            },{
              'name': 'iRecargoLEACS',
              'index': 'iRecargoLEACS',
              'align': 'right',              
              'width': 130,
              'editable': true,
              formatter:'currency',
              formatoptions: importeFormat,
              editoptions: {
                type: "number"
              }
            },{
              'name': 'iImpuesto',
              'index': 'iImpuesto',
              'align': 'right',              
              'width': 130,
              'editable': true ,              
              formatter:'currency',
              formatoptions: importeFormat,
              editoptions: {
                type: "number"
              }
            }],            
            'shrinkToFit': false,
            'loadonce': true,
            'rownumbers': false,
            'cmTemplate': { sortable: false, 'fixed': true },
            'width': 700,
            //'scroll': true,
            'rowNum': 100 ,
            onSelectRow: function (rowid, stat, e) {              

              if(rowid > 0 ) {
                $rootScope.$emit('enableGridWarrantyButtons', true);
              }else{
                $rootScope.$emit('enableGridWarrantyButtons', false);
              } 
            }
                                   
          }
        }

        function getProductorListOptions(){
          return {
            'datatype': 'local',
            'height': 'auto',
            'editurl': 'clientArray',
            'colNames': [
                $filter('translate')('altaApunte.productorId'),
                $filter('translate')('altaApunte.tipoIntervencion'),
                $filter('translate')('altaApunte.comision'),
                $filter('translate')('altaApunte.importe')
            ],
            'colModel': [{
              'name': 'productorId',
              'index': 'productorId',
              'align': 'left',
              'hidden':true               
            },{
              'name': 'kTipoIntervencion',
              'index': 'kTipoIntervencion',
              'align': 'left',                
              'editable': true,              
              editoptions:{
                maxlength: 2
              } 
            },{
              'name': 'pPorcentaje',
              'index': 'pPorcentaje',
              'align': 'right',
              'editable': true,
              formatter:'currency',
              formatoptions: importeFormat
            },{
              'name': 'iImporte',
              'index': 'iImporte',
              'align': 'right',
              'formatter': function(cellvalue, options, rowObject){
                return comunesSrv.formatMoney(cellvalue, '2', '.', ',');
              }              
            }],            
            'shrinkToFit': true,
            'forceFit': true,
            'rownumbers': false,
            'cmTemplate': { sortable: false },
            'rowNum': 100 ,
            //'scroll': true,
            onSelectRow: function (rowid, stat, e) {

              if(rowid > 0 ) {
                $rootScope.$emit('enableGridProdButtons', true);
              } 
            }
          }
        }

        function validateForm(myForm){

          var formAlta = JSON.parse(JSON.stringify(myForm)),
              errorArray = [],
              errorMaxlength = [],
              description = '',
              fieldCode = '',
              inputElement,
              maxlength;              

          // validate maxlength  
          if(formAlta.$error.hasOwnProperty('maxlength') && angular.isArray(formAlta.$error.maxlength)){

            formAlta.$error.maxlength.forEach(function (error) {
              
              inputElement = document.getElementsByName(error.$name);
              maxlength = angular.element(inputElement).attr('ng-maxlength'); 
              
              fieldCode = 'altaApunte.' + error.$name;            
              description = $filter('translate')(fieldCode) + ': ' + msgMaxlength + ' ' + maxlength;
              errorMaxlength.push({description: description, form: formAlta.$name, formControl: error.$name });
            });
          } 
          
          // validate required and pattern
          var errorRequired = validateRequiredAndNumber(formAlta, 'required'),
              errorPattern = validateRequiredAndNumber(formAlta, 'pattern');  
              
          errorArray = Object.assign({}, errorRequired, errorPattern, errorMaxlength);          
          return Object.keys(errorArray).map(function(x) { return errorArray[x]; })
        }

        function validateRequiredAndNumber(formAlta, typeError){
          var errorArray = [],
              fieldCode,
              description;

          if(formAlta.$error.hasOwnProperty(typeError) && angular.isArray(formAlta.$error[typeError])){
            
             formAlta.$error[typeError].forEach(function (error) {
               
               fieldCode = 'altaApunte.' + error.$name;
               if(error.$error.hasOwnProperty('number')){
                 description = $filter('translate')(fieldCode) + ': ' + msgNumber;
               }else{
                 description = $filter('translate')(fieldCode) + ': ' + msgRequired;
               }              
               
               errorArray.push({description: description, form: formAlta.$name, formControl: error.$name });
             });
           }
           
          return errorArray;
        }

        function validateWarrantyTable(){
          
          var warrantyGrid = comunesSrv.getGridDatas('idGridGuarantee'),
              errors = [],
              warrantyData = comunesSrv.cloneJson(warrantyGrid.datas),
              message = '',
              isEmpty = false,
              rowWarranty = {},
              typeError = {
                empty: false,
                sign: false,
                lengthImporte: false
              };

          if(warrantyData.length > 0){

            for(var i=0; i<warrantyData.length; i++){              

              rowWarranty = warrantyData[i];

              if(rowWarranty.hasOwnProperty('iPrimaTotal')){
                delete rowWarranty.iPrimaTotal;  
              }       

              // validate empty cells
              if(Object.keys(rowWarranty).length === warrantyGrid.colNames.length){

                var kindError = validateWarrantyCells(rowWarranty);
                // empty
                if(kindError.empty){
                  typeError.empty = true;
                }  
                // sign
                if(kindError.sign){
                  typeError.sign = true;
                }
                // max lenght import
                if(kindError.lengthImporte){
                  typeError.lengthImporte = true;
                }                 
                
              }else{
                typeError.empty = true;
              }
            }
          }

          if(typeError.empty){
            message = $filter('translate')('altaApunte.listaGarantia') + ": " + $filter('translate')('altaApunte.editableRequerido');
            errors.push({'description': message, 'form': 'altaImportesForm', 'formControl': 'idGridGuarantee' });             
          }

          if(typeError.sign){                        
            message = $filter('translate')('altaApunte.listaGarantia') + ": " + $filter('translate')('altaApunte.validateSignImporte');
            errors.push({'description': message, 'form': 'altaImportesForm', 'formControl': 'idGridGuarantee' });                         
          }

          if(typeError.lengthImporte){            
            message = $filter('translate')('altaApunte.listaGarantiaImporte') + ": " + $filter('translate')('mensaje.maxLengthImporte');
            errors.push({'description': message, 'form': 'altaImportesForm', 'formControl': 'idGridGuarantee' });                         
          }          

          return errors;
        }

        function validateProducerTable(){
          
          var gridEditApunte = comunesSrv.getGridDatas('idGridProductor'),
              errors = [],
              gridDatas = comunesSrv.cloneJson(gridEditApunte.datas),
              message = '',              
              rowData = {},
              cellError = {},
              percentage = 0,
              typeError = {
                empty: false
              };          

          for(var i=0; i<gridDatas.length; i++){              

            rowData = gridDatas[i];
            cellError = validateRowProducer(rowData);
            
            if(cellError.empty){
              typeError.empty = true;
            }
            
            percentage = percentage + parseFloat(rowData.pPorcentaje);
          }

          if(typeError.empty){
            message = $filter('translate')('altaApunte.listaProductores') + ": " +  $filter('translate')('altaApunte.productoresRequerido');
            errors.push({'description': message, 'form': 'altaImportesForm', 'formControl': 'idGridProductor' });             
          }          

          if(percentage > 100){            
            message = $filter('translate')('altaApunte.porcentajeProductores');
            errors.push({'description': message, 'form': 'altaImportesForm', 'formControl': 'idGridGuarantee' });                         
          }

          return errors;
        } 


        function validateRowProducer(rowData){
          
          var cellError = {
                empty: false                
              };          

          if(Object.keys(rowData).length >= 3){

            Object.keys(rowData).forEach(function(key) {
              
              var value = rowData[key];

              // validate empty values                            
              if(key !== 'iImporte' && comunesSrv.isNullEmpty(value)){
                cellError.empty = true;
              }
            })
            
          }else{
            cellError.empty = true;
          }
          
          return cellError;
        }        
        
        function validateWarrantyCells(obj){

          var isValid = false,
              countPositive = 0,
              countNegative = 0,
              lengthKeys = Object.keys(obj).length,
              typeError = {
                empty: false,
                sign: true,
                lengthImporte: false
              };

          Object.keys(obj).forEach(function(key) {
            
            var value = obj[key];

            // validate empty
            if(comunesSrv.isNullEmpty(value)){
              typeError.empty = true;
            }

            // validate maxlenght importe
            if(comunesSrv.countIntegerNumber(value) > maxLengthImporte ){  
              typeError.lengthImporte = true;
            }            

            // count positive and negative
            if(key !== "iDescuento" && 
            key !== "kGarantia" && 
            key !== "id"){

              if(Math.floor(Number(value)) > 0){
                countPositive = countPositive + 1;
              }else if(Math.floor(Number(value)) < 0){
                countNegative = countNegative + 1;
              }else{
                countNegative = countNegative + 1;
                countPositive = countPositive + 1;
              }
            }

          }); 
          
          var lengthRequired = lengthKeys - 3;

          if(countPositive === lengthRequired && Math.floor(Number(obj.iDescuento)) <= 0){
            typeError.sign = false;           
          }

          if(countNegative === lengthRequired && Math.floor(Number(obj.iDescuento)) >= 0){
            typeError.sign = false;           
          }          
        
          return typeError;
        }

        function validateImporteTotales(){

          var arrayInputName = ['iReajuste', 'iAnticipo', 'iRecobro'],
              formName = 'altaImportesForm',
              codeMultiLanguage = 'altaApunte';

          return validarFormSrv.validateInputs(arrayInputName, codeMultiLanguage, formName);           
        }

        function validateBeforeCalculating(){

          // save all rows mode editing
          comunesSrv.saveAllRowGrid("idGridGuarantee"); // lista garantias
          comunesSrv.saveAllRowGrid("idGridProductor"); // lista productores

          var errorTableWarranty = validateWarrantyTable(),
              errorTableProducer = validateProducerTable(),
              errorImporteTotales = validateImporteTotales(),
              errors = errorTableWarranty.concat(errorTableProducer, errorImporteTotales);
            
          return errors;
        }

        function validateImporteSign(model){

          var element = angular.element('#altaImportesForm'),
              countPositive = 0,
              countNegative = 0,
              countRelativePos = 0,
              countRelativeNeg = 0,              
              inputValue,
              inputId,
              msg,
              errors = [],
              nroImportes = 7,
              nroRelatives = 3;
              
          // validate same sign for importes
          angular.forEach(element.find('input'), function(inputElement){ 
            
            inputValue = inputElement.value;
            inputId = inputElement.id;
            inputValue = comunesSrv.parseCommaToDecimal(inputValue)

            // count importes positives and negatives 
            if(inputId !== 'iReajuste' && 
            inputId !== 'iAnticipo' && 
            inputId !== 'iTotalDescuento'){

              if(Math.floor(Number(inputValue)) > 0){
                countPositive = countPositive + 1;
              }else if(Math.floor(Number(inputValue)) < 0){
                countNegative = countNegative + 1;
              }else{
                countNegative = countNegative + 1;
                countPositive = countPositive + 1;
              }              
            }else{

              // count importes relatives positives and negatives  
              if(Math.floor(Number(inputValue)) > 0){
                countRelativePos = countRelativePos + 1;
              }else if(Math.floor(Number(inputValue)) < 0){
                countRelativeNeg = countRelativeNeg + 1;
              }else{
                countRelativeNeg = countRelativeNeg + 1;
                countRelativePos = countRelativePos + 1;
              }                

            }

          });


          if((countPositive === nroImportes && countRelativeNeg === nroRelatives) || 
          countNegative === nroImportes && countRelativePos === nroRelatives){          

          }else{
            msg = $filter('translate')('mensaje.importeTotalizado');              
            errors.push({description: msg, form: "none", formControl: 'none' }); 
          }

          return errors
        }

        return {
          getGuaranteeListOptions: getGuaranteeListOptions,
          getProductorListOptions: getProductorListOptions,          
          validateForm: validateForm,
          validateWarrantyTable: validateWarrantyTable,
          validateProducerTable: validateProducerTable,
          validateBeforeCalculating: validateBeforeCalculating,
          validateImporteSign: validateImporteSign
        }         

      }]);

})();
