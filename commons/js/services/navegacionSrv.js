/**
 * Created by Fred on 9/24/2017.
 */
(function () {
    'use strict';
    /* global appModule */
    (appModule.lazy || appModule)

      .factory('navegacionSrv', ['HttpSrv', function ( HttpSrv) {  
          
        var sessionList = [];
        
        function get(stateId) {
          var data = null;
          for (var index in sessionList)
          {
            if(sessionList[index].stateId === stateId){
              data = sessionList[index];
            }
          }

          return data;
        }          
        
        function set(stateId, session) {
          var objeto = {}
          objeto.stateId = stateId;
          objeto.session = JSON.parse(JSON.stringify(session));
          sessionList.push(objeto);
        }  
                
        return {
          get: get,
          set: set
        };       

    }]);

})();
