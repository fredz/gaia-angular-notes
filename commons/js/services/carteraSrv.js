/**
 * Created by Fred on 9/24/2017.
 */
(function () {
    'use strict';
    /* global appModule */
    (appModule.lazy || appModule)
        .factory('carteraSrv', ['HttpSrv', function ( HttpSrv) {  
          
          var url = '/api/consultaPolizaCartera';

          function getConsultaPolizaCartera(data){
            return HttpSrv.post(url, data);
          }       

          return {
            getConsultaPolizaCartera: getConsultaPolizaCartera
          }         

        }]);

})();
