/**
 * Created by Fred on 10/10/2017.
 */
(function () {
    'use strict';

    function drvTabDatosEconomicosCtrl($scope, altaApuntesSrv, apuntesSrv, 
      LoadingSrv, $filter, comunesSrv, 
      alertaApuntesSrv, $location, $anchorScroll, validarFormSrv) {

      var lastSelection; // for grid warranty datas
      $scope.guaranteeListOptions = altaApuntesSrv.getGuaranteeListOptions();

      // event to calculate apunte from parent control
      $scope.$on('executeCalcularApunte', function(event, args) {          
        var callbackFn = (args.callbackFn !== undefined)? args.callbackFn: null;
        $scope.runCalcularApunte(callbackFn);            
      });

      $scope.prepareCalcularApunte = function(){        

        alertaApuntesSrv.cleanFormAlert();
        var errors = altaApuntesSrv.validateBeforeCalculating();

        if(errors.length > 0){
          
          alertaApuntesSrv.addAlerts(errors);          
          $location.hash('end-main-container');                
          $anchorScroll();                
        }else{
          $scope.runCalcularApunte();          
        }
      }  

      $scope.runCalcularApunte = function(myCallback){
        
        LoadingSrv.set($filter('translate')('cargando'));
        var data = prepareDataCalcular();

        // fill out warranty datas for datos economicos
        apuntesSrv.calcularApunte(data).then(function(result){

          fillImportesData(result);
          fillWarrantyData(result);
          fillProducerData(result);

          // execute callback
          if (myCallback && typeof(myCallback) === "function"){            
            myCallback();
          }
          LoadingSrv.hide();
        });
      }        

      function fillProducerData(result){

        var tablaProducer = comunesSrv.getGridDatas('idGridProductor'),
            calculatedlistProd = result.apunte.listaProductores;

        // add calculated value
        if(tablaProducer.datas.length > 0){

          $scope.listaProductores = tablaProducer.datas;

          for(var i = 0; i < calculatedlistProd.length; i++){           

            if($scope.listaProductores[i] !== undefined){
              $scope.listaProductores[i].iImporte = calculatedlistProd[i].iImporte;
            }            
          }        
        }
      }

      function fillImportesData(result){

        $scope.model.fApunte = result.apunte.fApunte;
        $scope.model.cEstado = result.apunte.cEstado;
        $scope.model.cSituacion = result.apunte.cSituacion;

        // fill amounts disabled
        $scope.model.iTotalPrimaNeta = result.apunte.iTotalPrimaNeta;
        $scope.model.iTotalPrimaTotal = result.apunte.iTotalPrimaTotal;
        $scope.model.iTotalDescuento = result.apunte.iTotalDescuento;
        $scope.model.iTotalRecargo = result.apunte.iTotalRecargo;
        $scope.model.iTotalRECCS = result.apunte.iTotalRECCS;
        $scope.model.iTotalLEACS = result.apunte.iTotalLEACS;
        $scope.model.iTotalImpuesto = result.apunte.iTotalImpuesto;  
        
        // after loaded te data ensure currency format
        ensureCurrencyFormat();

      }

      function ensureCurrencyFormat(){

        setTimeout(function(){ 
          angular.element('#iTotalPrimaNeta').trigger('focus').trigger('blur');
          angular.element('#iTotalPrimaTotal').trigger('focus').trigger('blur');
          angular.element('#iTotalDescuento').trigger('focus').trigger('blur');
          angular.element('#iTotalRecargo').trigger('focus').trigger('blur');
          angular.element('#iTotalRECCS').trigger('focus').trigger('blur');
          angular.element('#iTotalLEACS').trigger('focus').trigger('blur');
          angular.element('#iTotalImpuesto').trigger('focus').trigger('blur');

        }, 1000);        

      }

      function fillWarrantyData(result){

        var tablaWarranty = comunesSrv.getGridDatas('idGridGuarantee'),        
            calculatedListWarranty = result.apunte.listaGarantias; // fill total

        if(tablaWarranty.datas.length > 0){

          $scope.listaGarantias = tablaWarranty.datas;

          // add calculated value
          for(var i = 0; i < calculatedListWarranty.length; i++){
            $scope.listaGarantias[i].iPrimaTotal = calculatedListWarranty[i].iPrimaTotal;
          }     
        }

      }

      $scope.modeGuaranteeEditable = function(){

        if($scope.enableWarrantyButtons === true){          

          var myGrid = angular.element('#idGridGuarantee'),
              selectedRowId = myGrid.jqGrid ('getGridParam', 'selrow');

          if (selectedRowId) {
            comunesSrv.saveAllRowGrid("idGridGuarantee");
            myGrid.jqGrid('editRow', selectedRowId);
          }
        }        
      }

      function prepareDataCalcular(){

        var warrantyData = comunesSrv.getGridDatas("idGridGuarantee"),
            producerData = comunesSrv.getGridDatas("idGridProductor");

        return {  
            "cIdioma":"ES",
            "apunte":{  
              "fApunte": $scope.model.fApunte,
              "cEstado": $scope.model.cEstado,
              "cSituacion":"DR",
              "iReajuste": $scope.model.iReajuste,
              "iAnticipo": $scope.model.iAnticipo,
              "iRecobro": $scope.model.iRecobro,
              "listaGarantias": warrantyData.datas,
              "listaProductores": producerData.datas
            }
        };        
      }      

    }

    /*global appModule */
    (appModule.lazy || appModule)
        .directive('drvTabDatosEconomicos', ['$window', function ($window) {
          
           return{
              restrict: 'AE',
              link: function(scope, elm, attrs) {
              },                          
              templateUrl: '/commons/html/drvTabDatosEconomicosView.html',
              controller: ['$scope', 'altaApuntesSrv', 'apuntesSrv', 
              'LoadingSrv', '$filter', 'comunesSrv',
              'alertaApuntesSrv', '$location', '$anchorScroll', 
              'validarFormSrv', drvTabDatosEconomicosCtrl]
           }
        }]);

})();
