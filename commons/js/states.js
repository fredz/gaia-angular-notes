/*global states */

(function (states) {
    'use strict';

    var appState,
        busquedaApuntes,        
        edicionApunte,
        copiarApunte,
        detalleApunte,
        emisionRecibo,
        altaApunte;


    appState = {
        'name': 'app',
        'abstract': true,
        'url': '',
        'templateUrl': 'commons/html/app.html'
    };
    states.push(appState);

    busquedaApuntes = {
        name:'busquedaApuntes',
        url:'/busquedaApuntes',
        templateUrl:'apuntesRecibo/busquedaApuntes/html/busquedaApuntesView.html',
        controller:'BusquedaApuntesCtrl',
        dictionary:'commons/i18n/messages.json',
        dependencies: [              
              'apuntesRecibo/js/apuntesRecibo.js'
        ]
    };
    states.push(busquedaApuntes);

    edicionApunte = {
      name:'edicionApunte',
      url:'/edicionApunte',
      templateUrl:'apuntesRecibo/edicionApunte/html/edicionApunteView.html',
      controller:'EdicionApunteCtrl',
      dictionary:'commons/i18n/messages.json',
      dependencies: [            
            'apuntesRecibo/js/apuntesRecibo.js'
      ]
    };
    states.push(edicionApunte);   
    
    copiarApunte = {
      name:'copiarApunte',
      url:'/copiarApunte',
      templateUrl:'apuntesRecibo/copiarApunte/html/copiarApunteView.html',
      controller:'CopiarApunteCtrl',
      dictionary:'commons/i18n/messages.json',
      dependencies: [            
            'apuntesRecibo/js/apuntesRecibo.js'
      ]
    };
    states.push(copiarApunte);    
    
    detalleApunte = {
      name:'detalleApunte',
      url:'/detalleApunte',
      templateUrl:'apuntesRecibo/detalleApunte/html/detalleApunteView.html',
      controller:'DetalleApunteCtrl',
      dictionary:'commons/i18n/messages.json',
      dependencies: [            
            'apuntesRecibo/js/apuntesRecibo.js'
      ]
    };
    states.push(detalleApunte);  
    
    emisionRecibo = {
      name:'emisionRecibo',
      url:'/emisionRecibo',
      templateUrl:'apuntesRecibo/emisionRecibo/html/emisionReciboView.html',
      controller:'EmisionReciboCtrl',
      dictionary:'commons/i18n/messages.json',
      dependencies: [            
            'apuntesRecibo/js/apuntesRecibo.js'
      ]
    };
    states.push(emisionRecibo);  

}(states));
