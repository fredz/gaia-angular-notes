/**
 * Created by Fred on 9/24/2017.
 */
(function () {
    'use strict';
    /* global appModule */
    (appModule.lazy || appModule)
        .factory('cambioFraccionamientoSrv', ['$modal', 'tablasGeneralesSrv', 'comunesSrv',
        '$filter', function ( $modal, tablasGeneralesSrv, comunesSrv, $filter) {  
          
            /* modal fractionation */
          function openFractionation(){
              
            return $modal.open({
                templateUrl: '/commons/html/modalCambioFraccionamientoView.html', // loads the template
                backdrop: true, // setting backdrop allows us to close the modal window on clicking outside the modal window
                windowClass: 'modal-fractionation', // windowClass - additional CSS class(es) to be added to a modal window template
                controller: [ '$modalInstance', '$scope', 'alertaApuntesSrv', 'tablasGeneralesSrv', 'LoadingSrv',
                  function ($modalInstance, $scope, alertaApuntesSrv, tablasGeneralesSrv, LoadingSrv) {

                    $scope.model = {
                      frFormaPago: '0',
                      frMetodo: '0'
                    }

                    $scope.formaPagoList = [];
                    $scope.metodoRegList = [];                    

                    // clean error alerts
                    alertaApuntesSrv.cleanFormAlert();

                    $scope.cancel = function () {                          
                        $modalInstance.dismiss('cancel'); 
                    };

                    $scope.accept = function(){
                      $scope.validateFractionationForm();
                    }

                    $scope.validateFractionationForm = function(){ 
                      var msg = '',
                          errors = [],
                          formName = 'noteFormFractionation';
          
                      alertaApuntesSrv.cleanFormAlert(); 
          
                      if($scope.model.frFormaPago.toString() === '0' || $scope.model.frFormaPago.toString() === ''){
                        msg = comunesSrv.msgFieldMandatory('fraccionamiento.frFormaPago');
                        errors.push({description: msg, form: formName, formControl: 'frFormaPago' });    
                      }
                      
                      if($scope.model.frMetodo.toString() === '0' || $scope.model.frMetodo.toString() === ''){
                        msg = comunesSrv.msgFieldMandatory('fraccionamiento.frMetodo');
                        errors.push({description: msg, form: formName, formControl: 'frMetodo' });    
                      }              
                        
                      if(errors.length > 0){
                        alertaApuntesSrv.addAlerts(errors)
                      }else{
                        $scope.cancel();
                      }
                    }                    
                    
                    $scope.loadGeneralTables = function(){
                      
                      LoadingSrv.set($filter('translate')('cargando'));
                      Promise
                        .all([
                          tablasGeneralesSrv.loadFormaPago(),
                          tablasGeneralesSrv.loadMetodoReg()])
                        .then(function(result) {
                          var formaPago = result[0],
                              metodoReg = result[1];
        
                          // way to pay                          
                          $scope.formaPagoList = comunesSrv.addSelecioneOption(formaPago);
                          
                          // method                          
                          $scope.metodoRegList = comunesSrv.addSelecioneOption(metodoReg);
        
                          LoadingSrv.hide();
                        })
                    }

                    $scope.loadGeneralTables();

                }]
            });//end of modal.open
          } // end of scope.open function

          return {
            openFractionation: openFractionation
          }; 

        }]);

})();
