(function (angular, states) {
    'use strict';

    var appModule = angular.module('gaiaapp', ['gaiafrontend', 'ui.bootstrap']);

    appModule
        .config(['$controllerProvider', '$compileProvider', '$filterProvider', '$provide', function ($controllerProvider, $compileProvider, $filterProvider, $provide) {
            // Expose AngularJS components registration methods so they can be lazy-loaded
            appModule.lazy = {
                controller: function () {
                    $controllerProvider.register.apply(this, arguments);
                    return appModule.lazy;
                },
                directive: function () {
                    $compileProvider.directive.apply(this, arguments);
                    return appModule.lazy;
                },
                filter: function () {
                    $filterProvider.register.apply(this, arguments);
                    return appModule.lazy;
                },
                constant: function () {
                    $provide.constant.apply(this, arguments);
                    return appModule.lazy;
                },
                factory: function () {
                    $provide.factory.apply(this, arguments);
                    return appModule.lazy;
                },
                provider: function () {
                    $provide.provider.apply(this, arguments);
                    return appModule.lazy;
                },
                service: function () {
                    $provide.service.apply(this, arguments);
                    return appModule.lazy;
                },
                value: function () {
                    $provide.value.apply(this, arguments);
                    return appModule.lazy;
                }
            };
        }])
        .config(['$urlRouterProvider', function ($urlRouterProvider) {
            // Redirect to '/greet' if unknown URL
            $urlRouterProvider.otherwise('/busquedaApuntes');
        }])
        // .config(['$locationProvider', function ($locationProvider) {
        //     // Allow HTML5 mode navigation (regular URL path and search segments instead of their hashbang equivalents)
        //     $locationProvider.html5Mode(true);
        // }])
        .config(['i18nProvider', '$translateProvider', function (i18nProvider, $translateProvider) {
            // $translateProvider.translations method wrapper to dynamically change dictionaries. It cannot be done by default
            // TODO: Create $translateProvider decorator but respect i18n factory.
            i18nProvider.setTranslateProviderTranslations($translateProvider.translations);
        }])
        .config(['StateProvider', '$stateProvider', function (StateProvider, $stateProvider) {
            // $stateProvider.states method wrapper to order and transform custom state objects
            StateProvider.resolve(states)($stateProvider);
        }])
        .config(['$httpProvider', function ($httpProvider) {
            // Set up $http interceptors
            $httpProvider.interceptors.push('i18nInterceptor', 'ErrorInterceptor','sessionInterceptor');
        }])
        .config(['HttpSrvConfig', function (HttpSrvConfig) {
            // HttpSrv set up
            HttpSrvConfig.wrapRequests = true;
            HttpSrvConfig.unwrapResponses = true;
        }])
        .config(['SessionConfig', function(SessionConfig){
            SessionConfig.time = 1;
            SessionConfig.sessionParams.sessionAlertTime = 600000;
            SessionConfig.sessionParams.endPoint = 'api/session';
            SessionConfig.sessionParams.expiredHREF = '/';
        }])
        .constant('rutaApuntesRecibo', {url:'[rutaApuntesRecibo]'})
        .run(['Language', '$rootScope', '$state',
            function (Language, $rootScope, $state) {
                // Reload current state when apoplication language is changed via Language factory
                // ui.router default reload method is not working as intended
                $rootScope.$on(Language.LANGUAGE_CHANGED_EVENT, function () {
                    if ($state.current['abstract']) return;
                    $state.transitionTo($state.current, $state.params, {
                        reload: true
                    });
                });
            }]);

}(this.angular, this.states));
