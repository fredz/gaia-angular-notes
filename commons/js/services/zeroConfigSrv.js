/**
 * Created by Fred on 9/24/2017.
 */
(function () {
    'use strict';
    /* global appModule */
    (appModule.lazy || appModule)
        .factory('zeroConfigSrv', ['HttpSrv', function ( HttpSrv) {  
          
          var url = '/api/zeroConfig',
              config = {cache: true};

          function getZeroConfig(){
            return HttpSrv.get(url, config);
          }       

          return {
            getZeroConfig: getZeroConfig
          };         

        }]);

})();
