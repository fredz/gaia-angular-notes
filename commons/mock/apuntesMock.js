/**
 * Created by Relvin on 9/19/2017.
 */
(function () {
  'use strict';
  var DELAY = 1000,
      THEME = "",
      TOKEN = "1234567890",
      rutaApuntesRecibo,
      cIdioma;

  function json(res, data) {
      setTimeout(function() {
          res.set({
              "X-User-theme": THEME
          });
          res.cookie('csrfCookie', TOKEN);
          res.json(data);
      }, DELAY);
  }

  exports.getUserInfo = {
      method: 'post',
      url: '/api/userInfo',
      mock: function(req, res) {
          json(res, {
              "data": {
                  "name": "World",
                  "firstLastName": "",
                  "secondLastName": "",
                  "dateOfBirth": -622598400000,
                  "clientEmail": "john.doe@AcmeCorp.com",
                  "nif": "01234567-A",
                  "applicationData": {
                      "language": "en"
                  }
              }
          });
      }
  };

  exports.detalleApunte = {
    method: 'post',
    url: '/api/detalleApunte',
    mock: function(req, res) {
        json(res, {
          "data":{  
            "retorno":{  
               "cRetorno":"00",
               "gRetorno":"",
               "cPrograma":""
            },
            "apunte":{  
               "datosApunte":{  
                  "nPagPoliza":"0007000014",
                  "nPagSubpoliza":"0000000",
                  "nPagCertificado":"000000",
                  "kPoliza":"0007000014",
                  "kSubpoliza":"0000000",
                  "kSuplemento":"000001",
                  "kCertificado":"000000",
                  "kVersion":"001",
                  "fApunte":"01/01/2017",
                  "cCompania":"G16398745",
                  "cNegocio":"CO",
                  "cOrigen":"NP",
                  "cOperacion":"NP",
                  "cTipoApunte":"MA",
                  "fFechaDesde":"01/01/2017",
                  "fFechaHasta":"01/02/2017",
                  "nDiasDiferemiento":"03",
                  "cTipoSoporte":"P",
                  "cCobroAutomatico":false,
                  "cEstado":"PE",
                  "cSituacion":"DE",
                  "gCausa":"",
                  "gObservaciones":"Manual",
                  "iReajuste":"-100,00",
                  "iAnticipo":"-100,00",
                  "iRecobro":"-100,00",
                  "iTotalPrimaNeta":"300,00",
                  "iTotalPrimaTotal":"2000,00",
                  "iTotalDescuento":"-300,00",
                  "iTotalRecargo":"900,00",
                  "iTotalRECCS":"300,00",
                  "iTotalLEACS":"300,00",
                  "iTotalImpuesto":"300,00",
                  "cAdhVol":false
               },
                "listaGarantias":[  
            {  
              "kGarantia":"1",
              "iPrimaNeta":"100,00",
              "iPrimaTotal":"200,00",
              "iDescuento":"-10,00",
              "iRecargoPagoFraccionado":"10,00",
              "iRECCS":"10,00",
              "iLEACS":"10,00",
              "iImpuesto":"10,00",
              "iRecargoRECCS":"10,00",
              "iRecargoLEACS":"60,00"
            },
            {  
              "kGarantia":"2",
              "iPrimaNeta":"200,00",
              "iPrimaTotal":"300,00",
              "iDescuento":"-10,00",
              "iRecargoPagoFraccionado":"10,00",
              "iRECCS":"10,00",
              "iLEACS":"10,00",
              "iImpuesto":"10,00",
              "iRecargoRECCS":"10,00",
              "iRecargoLEACS":"60,00"
            }
          ],
               "listaProductores":[  
                  {  
              "kTipoIntervencion":"AG",
              "pPorcentaje":"6,00",
              "iImporte":"150,56"
            },
            {  
              "kTipoIntervencion":"PR",
              "pPorcentaje":"2,00",
              "iImporte":"60,00"
            }
               ]
            }
         }
        });
    }
  };

  exports.editApunte = {
    method: 'post',
    url: '/api/editarApunte',
    mock: function(req, res) {
        json(res, {
          "data":{  
            "retorno":{  
              "cRetorno":"00",
              "gRetorno":"",
              "cPrograma":""
            }
          }
        });
    }
  };

  exports.altaApunte = {
    method: 'post',
    url: '/api/apunte',
    mock: function(req, res) {
        json(res, {
          "data":{  
            "retorno":{  
              "cRetorno":"00",
              "gRetorno":"",
              "cPrograma":""
            }
          }
        });
    }
  };  

  exports.calcularApunte = {
    method: 'post',
    url: '/api/calcularApunte',
    mock: function(req, res) {
        json(res,
          {
            "data":{  
              "retorno":{  
                 "cRetorno":"00",
                 "gRetorno":"",
                 "cPrograma":""
              },
              "apunte":{  
                "fApunte":"01/06/2018",
                "cEstado":"PE",
                "cSituacion":"DR",
                "iTotalPrimaNeta":"3000,00",
                "iTotalPrimaTotal":"1800,00",
                "iTotalDescuento":"-300,00",
                "iTotalRecargo":"9500,00",
                "iTotalRECCS":"300,00",
                "iTotalLEACS":"300,00",
                "iTotalImpuesto":"300,00",
              "listaGarantias":[  
              {  
                "kGarantia":"1",
                "iPrimaTotal":"600,00"
              },
              {  
                "kGarantia":"2",
                "iPrimaTotal":"1200,00"
              }
             ],
             "listaProductores":[  
              {  
                "kTipoIntervencion":"AG",
                "iImporte":"65874,84"
              },
              {  
                "kTipoIntervencion":"PR",
                "iImporte":"2541,87"
              }
             ]
              }
           }            
          }
        );
    }
  };   

  exports.consultaPolizaCartera = {
    method: 'post',
    url: '/api/consultaPolizaCartera',
    mock: function(req, res) {
      json(res,
        {
          "data":{  
            "retorno":{  
               "cRetorno":"00",
               "gRetorno":"",
               "cPrograma":""
            },
            "poliza":{  
               "cCompania":"G16398745",
               "fFechaRenovacion":"01/02/2018",
               "fEfectoSuplemento":"01/01/2018",
               "listaGarantias":[  
                  {  
                     "kGarantia":"1"
                  },
                  {  
                     "kGarantia":"2"
                  }
               ],
               "listaProductores":[  
                  { 
                    "productorId": "1", 
                    "kTipoIntervencion":"AG",
                    "pPorcentaje":"5.00"
                  },
                  {  
                    "productorId": "2", 
                    "kTipoIntervencion":"PR",
                    "pPorcentaje":"2.50"
                  }
               ]
            }
          }          
        }      
      );
    }
  };  

  exports.loadTipoSoporte = {
    method: 'get',
    url: '/api/js_listas_opciones_vida/t01020l',
    mock: function(req, res) {
        json(res,
          [
            //{"COD_VALOR":"CA","NOM_VALOR":"CARGO EN CUENTA"},
            {"COD_VALOR":"P","NOM_VALOR":"PAPEL"},
            //{"COD_VALOR":"RV","NOM_VALOR":"RECIBO A VALIDAR"},
            {"COD_VALOR":"S","NOM_VALOR":"SOPORTE"}
            //{"COD_VALOR":"TJ","NOM_VALOR":"TARJETA"},
            //{"COD_VALOR":"TO","NOM_VALOR":"TODOS"}
          ]      
        );
    }
  };  

  exports.loadTipoApunte = {
    method: 'get',
    url: '/api/js_listas_opciones_vida/t000004',
    mock: function(req, res) {
        json(res,
          [  
            {  
               "COD_VALOR":"MA",
               "NOM_VALOR":"Manual"
            },
            {  
               "COD_VALOR":"CA",
               "NOM_VALOR":"Cartera"
            }
         ]        
        );
    }
  }; 
  
  exports.loadTipoOperacion = {
    method: 'get',
    url: '/api/js_listas_opciones_vida/t000005',
    mock: function(req, res) {
        json(res,
          [  
            {  
               "COD_VALOR":"SU",
               "NOM_VALOR":"Suplemento"
            },
            {  
               "COD_VALOR":"NP",
               "NOM_VALOR":"Nueva producción"
            },
            {  
               "COD_VALOR":"RN",
               "NOM_VALOR":"Renovación"
            }
         ]       
        );
    }
  };   

  exports.loadOrigen = {
    method: 'get',
    url: '/api/js_listas_opciones_vida/t000002',
    mock: function(req, res) {
        json(res,
          [  
            {  
               "COD_VALOR":"NP",
               "NOM_VALOR":"Nueva producción"
            },
            {  
               "COD_VALOR":"CA",
               "NOM_VALOR":"Cartera"
            }
         ]          
        );
    }
  };   

  exports.getUsuarioVida = {
    method: 'get',
    url: '/api/objUser/user/cUsuario/info',
    mock: function(req, res) {
        json(res, {
          "data":{  
            "usuario":"USUARIO",
            "compania":1,
            "idioma":"ES",
            "codigoOficina":"9933",
            "nombreCortoOficina":"DESARROLLO",
            "direccionRegional":"99",
            "codigoActuacion":"99",
            "tipoOficina":"0",
            "clavePersona":1155870,
            "nif":"000000000Z",
            "funcionalidades":[  
               500,
               503,
               504,
               507,
               508,
               584,
               585,
               '0S002',
               '0S005'
            ],
            "canales":[  
               1,
               20,
               30,
               40,
               50,
               60,
               70,
               72,
               73
            ],
            "companies":[  
               10,
               20,
               40,
               60,
               70,
               72,
               73
            ],
            "codigoIntervencion":"",
            "claseAgente":"",
            "claveProductor":0,
            "fechaBaja":"00000000",
            "modelo":89,
            "ambito":20,
            "canalOficina":10,
            "direccionGeneralTerritorial":"99"
         }
        });
    }
  }; 
  
  exports.getLoguedUser = {
    method: 'get',
    url: rutaApuntesRecibo+'/api/loguedUser',
    mock: function(req, res) {
        json(res, {
          "data":"USUARIO"
        });
    }
  };   

  exports.loadBusiness = {
    method: 'get',
    url: '/api/js_listas_opciones_vida/t000003',
    mock: function(req, res) {
        json(res,
          [  
            {  
               "COD_VALOR":"CO",
               "NOM_VALOR":"Colectivos"
            },
            {  
               "COD_VALOR":"IN",
               "NOM_VALOR":"Individuales"
            }
         ]          
        );
    }
  }; 
  
  exports.loadStates = {
    method: 'get',
    url: '/api/js_listas_opciones_vida/t000001',
    mock: function(req, res) {
        json(res,
          [  
            {  
               "COD_VALOR":"PE",
               "NOM_VALOR":"Pendiente"
            },
            {  
               "COD_VALOR":"EM",
               "NOM_VALOR":"Emitido"
            },
            {  
               "COD_VALOR":"AN",
               "NOM_VALOR":"Anulado"
            }
         ]         
        );
    }
  };
  
  exports.loadSituation = {
    method: 'get',
    url: '/api/js_listas_opciones_vida/t000011',
    mock: function(req, res) {
        json(res,
          [  
            {  
               "COD_VALOR":"DE",
               "NOM_VALOR":"De emitir"
            },
            {  
               "COD_VALOR":"DR",
               "NOM_VALOR":"De renovación"
            },
            {  
               "COD_VALOR":"AR",
               "NOM_VALOR":"A renovación"
            }
         ]        
        );
    }
  }; 
  
  exports.loadCompanies = {
    //url: '/api/listadoCompanias/{cIdioma}',
    method: 'get',
    url: '/api/listadoCompanias',
    mock: function(req, res) {
        json(res,
          {  
            "data":{  
               "retorno":{  
                  "cRetorno":"00",
                  "gRetorno":"",
                  "cPrograma":""
               },
               "listadoCompanias":[  
                  {  
                     "cCompania":"10",
                     "cCIF":"G16398745",
                     "gDescripcion":"Compañía 1"
                  },
                  {  
                     "cCompania":"20",
                     "cCIF":"H03605243",
                     "gDescripcion":"Compañía 2"
                  }
               ]
            }
         }       
        );
    }
  };
  
  exports.loadMetodoReg = {    
    method: 'get',
    url: '/api/js_listas_opciones_vida/t000012',
    mock: function(req, res) {
        json(res,
          [  
            {  
               "COD_VALOR":"00",
               "NOM_VALOR":"Recibo inmediato"
            },
            {  
               "COD_VALOR":"01",
               "NOM_VALOR":"Próxima renovación"
            },
            {  
               "COD_VALOR":"02",
               "NOM_VALOR":"Vencimientos pendientes"
            },
            {  
               "COD_VALOR":"03",
               "NOM_VALOR":"Recibo inmediato y vencimientos pendientes"
            },
            {  
               "COD_VALOR":"09",
               "NOM_VALOR":"Recibo inmediato y vencimiento de póliza"
            }
         ]   
        );
    }
  }; 
  
  exports.loadFormaPago = {    
    method: 'get',
    url: '/api/js_listas_opciones_vida/t00013l',
    mock: function(req, res) {
        json(res,
          [  
            {"COD_VALOR":"1","NOM_VALOR":"ANUAL"},
            {"COD_VALOR":"2","NOM_VALOR":"SEMESTRAL"},
            {"COD_VALOR":"3","NOM_VALOR":"TRIMESTRAL"},
            {"COD_VALOR":"4","NOM_VALOR":"MENSUAL"},
            {"COD_VALOR":"9","NOM_VALOR":"PAGO UNICO"}
          ]      
        );
    }
  };   

  exports.getApuntes = {
    method: 'post',
    url: '/api/apuntes',
    mock: function(req, res) {
        json(res, apuntes());
    }
  };

  exports.getZeroConfig = {    
    method: 'get',
    url: '/api/zeroConfig',
    mock: function(req, res) {
        json(res,
          {  
            "data":{  
              "zeroConfig":{  
                  "URLApuntesRecibo":"wportalinerno.desa.net/ApuntesRecibo_fe-web"
              }
           }
         }       
        );
    }
  };   


  // lista de apuntes
  function apuntes() {
    return {
      "data":{  
        "retorno":{  
           "cRetorno":"00",
           "gRetorno":"",
           "cPrograma":""
        },
        "listaApuntes":[  
           {
              "cEstado":"PE",
              "cSituacion":"DE",
              "fApunte":"01/01/2017",
              "iPrimaTotal":"1000,00",
              "iReajuste":"100,00",
              "cTipoApunte":"CA",
              "nPagPoliza":"0007000014",
              "nPagSubpoliza":"0000000",
              "nPagCertificado":"000000",
              "kPoliza":"0007000014",
              "kSubpoliza":"0000000",
              "kSuplemento":"000001",
              "kCertificado":"000000",
              "kVersion":"001"
           },{  
              "cEstado":"PE",
              "cSituacion":"DE",
              "fApunte":"01/02/2017",
              "iPrimaTotal":"1000,00",
              "iReajuste":"100,00",
              "cTipoApunte":"CA",
              "nPagPoliza":"0007000014",
              "nPagSubpoliza":"0000000",
              "nPagCertificado":"000000",
              "kPoliza":"0007000014",
              "kSubpoliza":"0000000",
              "kSuplemento":"000001",
              "kCertificado":"000000",
              "kVersion":"001"
           },{  
              "cEstado":"AN",
              "cSituacion":"DE",
              "fApunte":"01/03/2017",
              "iPrimaTotal":"1000,00",
              "iReajuste":"100,00",
              "cTipoApunte":"CA",
              "nPagPoliza":"0007000014",
              "nPagSubpoliza":"0000000",
              "nPagCertificado":"000000",
              "kPoliza":"0007000014",
              "kSubpoliza":"0000000",
              "kSuplemento":"000001",
              "kCertificado":"000000",
              "kVersion":"001"
           },{  
              "cEstado":"PE",
              "cSituacion":"DE",
              "fApunte":"01/04/2017",
              "iPrimaTotal":"1000,00",
              "iReajuste":"100,00",
              "cTipoApunte":"CA",
              "nPagPoliza":"0007000014",
              "nPagSubpoliza":"0000000",
              "nPagCertificado":"000000",
              "kPoliza":"0007000014",
              "kSubpoliza":"0000000",
              "kSuplemento":"000001",
              "kCertificado":"000000",
              "kVersion":"001"
           },{  
              "cEstado":"AN",
              "cSituacion":"DE",
              "fApunte":"01/05/2017",
              "iPrimaTotal":"1000,00",
              "iReajuste":"100,00",
              "cTipoApunte":"CA",
              "nPagPoliza":"0007000014",
              "nPagSubpoliza":"0000000",
              "nPagCertificado":"000000",
              "kPoliza":"0007000014",
              "kSubpoliza":"0000000",
              "kSuplemento":"000001",
              "kCertificado":"000000",
              "kVersion":"001"
            },{  
              "cEstado":"PE",
              "cSituacion":"DE",
              "fApunte":"01/06/2017",
              "iPrimaTotal":"1000,00",
              "iReajuste":"100,00",
              "cTipoApunte":"CA",
              "nPagPoliza":"0007000014",
              "nPagSubpoliza":"0000000",
              "nPagCertificado":"000000",
              "kPoliza":"0007000014",
              "kSubpoliza":"0000000",
              "kSuplemento":"000001",
              "kCertificado":"000000",
              "kVersion":"001"
            },{  
              "cEstado":"PE",
              "cSituacion":"DE",
              "fApunte":"01/07/2017",
              "iPrimaTotal":"5000,00",
              "iReajuste":"100,00",
              "cTipoApunte":"CA",
              "nPagPoliza":"0007000014",
              "nPagSubpoliza":"0000000",
              "nPagCertificado":"000000",
              "kPoliza":"0007000014",
              "kSubpoliza":"0000000",
              "kSuplemento":"000001",
              "kCertificado":"000000",
              "kVersion":"001"
            },{  
              "cEstado":"PE",
              "cSituacion":"DE",
              "fApunte":"01/08/2017",
              "iPrimaTotal":"1000,00",
              "iReajuste":"100,00",
              "cTipoApunte":"CA",
              "nPagPoliza":"0007000014",
              "nPagSubpoliza":"0000000",
              "nPagCertificado":"000000",
              "kPoliza":"0007000014",
              "kSubpoliza":"0000000",
              "kSuplemento":"000001",
              "kCertificado":"000000",
              "kVersion":"001"
            },{  
              "cEstado":"PE",
              "cSituacion":"DE",
              "fApunte":"01/09/2017",
              "iPrimaTotal":"1000,00",
              "iReajuste":"100,00",
              "cTipoApunte":"CA",
              "nPagPoliza":"0007000014",
              "nPagSubpoliza":"0000000",
              "nPagCertificado":"000000",
              "kPoliza":"0007000014",
              "kSubpoliza":"0000000",
              "kSuplemento":"000001",
              "kCertificado":"000000",
              "kVersion":"001"
            },{  
              "cEstado":"PE",
              "cSituacion":"DE",
              "fApunte":"01/10/2017",
              "iPrimaTotal":"1000,00",
              "iReajuste":"100,00",
              "cTipoApunte":"CA",
              "nPagPoliza":"0007000014",
              "nPagSubpoliza":"0000000",
              "nPagCertificado":"000000",
              "kPoliza":"0007000014",
              "kSubpoliza":"0000000",
              "kSuplemento":"000001",
              "kCertificado":"000000",
              "kVersion":"001"
            },{  
              "cEstado":"PE",
              "cSituacion":"DE",
              "fApunte":"01/11/2017",
              "iPrimaTotal":"1000,00",
              "iReajuste":"100,00",
              "cTipoApunte":"CA",
              "nPagPoliza":"0007000014",
              "nPagSubpoliza":"0000000",
              "nPagCertificado":"000000",
              "kPoliza":"0007000014",
              "kSubpoliza":"0000000",
              "kSuplemento":"000001",
              "kCertificado":"000000",
              "kVersion":"001"
            },{  
              "cEstado":"PE",
              "cSituacion":"DE",
              "fApunte":"01/12/2017",
              "iPrimaTotal":"1000,00",
              "iReajuste":"100,00",
              "cTipoApunte":"CA",
              "nPagPoliza":"0007000014",
              "nPagSubpoliza":"0000000",
              "nPagCertificado":"000000",
              "kPoliza":"0007000014",
              "kSubpoliza":"0000000",
              "kSuplemento":"000001",
              "kCertificado":"000000",
              "kVersion":"001"
            },{  
              "cEstado":"PE",
              "cSituacion":"DE",
              "fApunte":"01/13/2017",
              "iPrimaTotal":"1000,00",
              "iReajuste":"100,00",
              "cTipoApunte":"CA",
              "nPagPoliza":"0007000014",
              "nPagSubpoliza":"0000000",
              "nPagCertificado":"000000",
              "kPoliza":"0007000014",
              "kSubpoliza":"0000000",
              "kSuplemento":"000001",
              "kCertificado":"000000",
              "kVersion":"001"
            }/*,{  
              "cEstado":"PE",
              "cSituacion":"DE",
              "fApunte":"01/14/2017",
              "iPrimaTotal":"1000,00",
              "iReajuste":"100,00",
              "cTipoApunte":"CA",
              "nPagPoliza":"0007000014",
              "nPagSubpoliza":"0000000",
              "nPagCertificado":"000000",
              "kPoliza":"0007000014",
              "kSubpoliza":"0000000",
              "kSuplemento":"000001",
              "kCertificado":"000000",
              "kVersion":"001"
            },{  
              "cEstado":"PE",
              "cSituacion":"DE",
              "fApunte":"01/15/2017",
              "iPrimaTotal":"1000,00",
              "iReajuste":"100,00",
              "cTipoApunte":"CA",
              "nPagPoliza":"0007000014",
              "nPagSubpoliza":"0000000",
              "nPagCertificado":"000000",
              "kPoliza":"0007000014",
              "kSubpoliza":"0000000",
              "kSuplemento":"000001",
              "kCertificado":"000000",
              "kVersion":"001"
            },{  
              "cEstado":"PE",
              "cSituacion":"DE",
              "fApunte":"01/16/2017",
              "iPrimaTotal":"1000,00",
              "iReajuste":"100,00",
              "cTipoApunte":"CA",
              "nPagPoliza":"0007000014",
              "nPagSubpoliza":"0000000",
              "nPagCertificado":"000000",
              "kPoliza":"0007000014",
              "kSubpoliza":"0000000",
              "kSuplemento":"000001",
              "kCertificado":"000000",
              "kVersion":"001"
            },{  
              "cEstado":"PE",
              "cSituacion":"DE",
              "fApunte":"01/17/2017",
              "iPrimaTotal":"1000,00",
              "iReajuste":"100,00",
              "cTipoApunte":"CA",
              "nPagPoliza":"0007000014",
              "nPagSubpoliza":"0000000",
              "nPagCertificado":"000000",
              "kPoliza":"0007000014",
              "kSubpoliza":"0000000",
              "kSuplemento":"000001",
              "kCertificado":"000000",
              "kVersion":"001"
            },{  
              "cEstado":"PE",
              "cSituacion":"DE",
              "fApunte":"01/18/2017",
              "iPrimaTotal":"1000,00",
              "iReajuste":"100,00",
              "cTipoApunte":"CA",
              "nPagPoliza":"0007000014",
              "nPagSubpoliza":"0000000",
              "nPagCertificado":"000000",
              "kPoliza":"0007000014",
              "kSubpoliza":"0000000",
              "kSuplemento":"000001",
              "kCertificado":"000000",
              "kVersion":"001"
            },{  
              "cEstado":"PE",
              "cSituacion":"DE",
              "fApunte":"01/19/2017",
              "iPrimaTotal":"1000,00",
              "iReajuste":"100,00",
              "cTipoApunte":"CA",
              "nPagPoliza":"0007000014",
              "nPagSubpoliza":"0000000",
              "nPagCertificado":"000000",
              "kPoliza":"0007000014",
              "kSubpoliza":"0000000",
              "kSuplemento":"000001",
              "kCertificado":"000000",
              "kVersion":"001"
            },{  
              "cEstado":"PE",
              "cSituacion":"DE",
              "fApunte":"01/20/2017",
              "iPrimaTotal":"1000,00",
              "iReajuste":"100,00",
              "cTipoApunte":"CA",
              "nPagPoliza":"0007000014",
              "nPagSubpoliza":"0000000",
              "nPagCertificado":"000000",
              "kPoliza":"0007000014",
              "kSubpoliza":"0000000",
              "kSuplemento":"000001",
              "kCertificado":"000000",
              "kVersion":"001"
            },{  
              "cEstado":"PE",
              "cSituacion":"DE",
              "fApunte":"01/21/2017",
              "iPrimaTotal":"1000,00",
              "iReajuste":"100,00",
              "cTipoApunte":"CA",
              "nPagPoliza":"0007000014",
              "nPagSubpoliza":"0000000",
              "nPagCertificado":"000000",
              "kPoliza":"0007000014",
              "kSubpoliza":"0000000",
              "kSuplemento":"000001",
              "kCertificado":"000000",
              "kVersion":"001"
            },{  
              "cEstado":"PE",
              "cSituacion":"DE",
              "fApunte":"01/22/2017",
              "iPrimaTotal":"1000,00",
              "iReajuste":"100,00",
              "cTipoApunte":"CA",
              "nPagPoliza":"0007000014",
              "nPagSubpoliza":"0000000",
              "nPagCertificado":"000000",
              "kPoliza":"0007000014",
              "kSubpoliza":"0000000",
              "kSuplemento":"000001",
              "kCertificado":"000000",
              "kVersion":"001"
            },{  
              "cEstado":"PE",
              "cSituacion":"DE",
              "fApunte":"01/23/2017",
              "iPrimaTotal":"1000,00",
              "iReajuste":"100,00",
              "cTipoApunte":"CA",
              "nPagPoliza":"0007000014",
              "nPagSubpoliza":"0000000",
              "nPagCertificado":"000000",
              "kPoliza":"0007000014",
              "kSubpoliza":"0000000",
              "kSuplemento":"000001",
              "kCertificado":"000000",
              "kVersion":"001"
            },{  
              "cEstado":"PE",
              "cSituacion":"DE",
              "fApunte":"01/24/2017",
              "iPrimaTotal":"1000,00",
              "iReajuste":"100,00",
              "cTipoApunte":"CA",
              "nPagPoliza":"0007000014",
              "nPagSubpoliza":"0000000",
              "nPagCertificado":"000000",
              "kPoliza":"0007000014",
              "kSubpoliza":"0000000",
              "kSuplemento":"000001",
              "kCertificado":"000000",
              "kVersion":"001"
            },{  
              "cEstado":"PE",
              "cSituacion":"DE",
              "fApunte":"01/25/2017",
              "iPrimaTotal":"1000,00",
              "iReajuste":"100,00",
              "cTipoApunte":"CA",
              "nPagPoliza":"0007000014",
              "nPagSubpoliza":"0000000",
              "nPagCertificado":"000000",
              "kPoliza":"0007000014",
              "kSubpoliza":"0000000",
              "kSuplemento":"000001",
              "kCertificado":"000000",
              "kVersion":"001"
            },{  
              "cEstado":"PE",
              "cSituacion":"DE",
              "fApunte":"01/26/2017",
              "iPrimaTotal":"1000,00",
              "iReajuste":"100,00",
              "cTipoApunte":"CA",
              "nPagPoliza":"0007000014",
              "nPagSubpoliza":"0000000",
              "nPagCertificado":"000000",
              "kPoliza":"0007000014",
              "kSubpoliza":"0000000",
              "kSuplemento":"000001",
              "kCertificado":"000000",
              "kVersion":"001"
            },{  
              "cEstado":"PE",
              "cSituacion":"DE",
              "fApunte":"01/27/2017",
              "iPrimaTotal":"1000,00",
              "iReajuste":"100,00",
              "cTipoApunte":"CA",
              "nPagPoliza":"0007000014",
              "nPagSubpoliza":"0000000",
              "nPagCertificado":"000000",
              "kPoliza":"0007000014",
              "kSubpoliza":"0000000",
              "kSuplemento":"000001",
              "kCertificado":"000000",
              "kVersion":"001"
            },{  
              "cEstado":"PE",
              "cSituacion":"DE",
              "fApunte":"01/28/2017",
              "iPrimaTotal":"1000,00",
              "iReajuste":"100,00",
              "cTipoApunte":"CA",
              "nPagPoliza":"0007000014",
              "nPagSubpoliza":"0000000",
              "nPagCertificado":"000000",
              "kPoliza":"0007000014",
              "kSubpoliza":"0000000",
              "kSuplemento":"000001",
              "kCertificado":"000000",
              "kVersion":"001"
            },{  
              "cEstado":"PE",
              "cSituacion":"DE",
              "fApunte":"01/29/2017",
              "iPrimaTotal":"1000,00",
              "iReajuste":"100,00",
              "cTipoApunte":"CA",
              "nPagPoliza":"0007000014",
              "nPagSubpoliza":"0000000",
              "nPagCertificado":"000000",
              "kPoliza":"0007000014",
              "kSubpoliza":"0000000",
              "kSuplemento":"000001",
              "kCertificado":"000000",
              "kVersion":"001"
            },{  
              "cEstado":"PE",
              "cSituacion":"DE",
              "fApunte":"02/01/2017",
              "iPrimaTotal":"1000,00",
              "iReajuste":"100,00",
              "cTipoApunte":"CA",
              "nPagPoliza":"0007000014",
              "nPagSubpoliza":"0000000",
              "nPagCertificado":"000000",
              "kPoliza":"0007000014",
              "kSubpoliza":"0000000",
              "kSuplemento":"000001",
              "kCertificado":"000000",
              "kVersion":"001"
            },{  
              "cEstado":"PE",
              "cSituacion":"DE",
              "fApunte":"02/02/2017",
              "iPrimaTotal":"1000,00",
              "iReajuste":"100,00",
              "cTipoApunte":"CA",
              "nPagPoliza":"0007000014",
              "nPagSubpoliza":"0000000",
              "nPagCertificado":"000000",
              "kPoliza":"0007000014",
              "kSubpoliza":"0000000",
              "kSuplemento":"000001",
              "kCertificado":"000000",
              "kVersion":"001"
            },{  
              "cEstado":"PE",
              "cSituacion":"DE",
              "fApunte":"02/03/2017",
              "iPrimaTotal":"1000,00",
              "iReajuste":"100,00",
              "cTipoApunte":"CA",
              "nPagPoliza":"0007000014",
              "nPagSubpoliza":"0000000",
              "nPagCertificado":"000000",
              "kPoliza":"0007000014",
              "kSubpoliza":"0000000",
              "kSuplemento":"000001",
              "kCertificado":"000000",
              "kVersion":"001"
            },{  
              "cEstado":"PE",
              "cSituacion":"DE",
              "fApunte":"02/04/2017",
              "iPrimaTotal":"1000,00",
              "iReajuste":"100,00",
              "cTipoApunte":"CA",
              "nPagPoliza":"0007000014",
              "nPagSubpoliza":"0000000",
              "nPagCertificado":"000000",
              "kPoliza":"0007000014",
              "kSubpoliza":"0000000",
              "kSuplemento":"000001",
              "kCertificado":"000000",
              "kVersion":"001"
            },{  
              "cEstado":"PE",
              "cSituacion":"DE",
              "fApunte":"02/05/2017",
              "iPrimaTotal":"1000,00",
              "iReajuste":"100,00",
              "cTipoApunte":"CA",
              "nPagPoliza":"0007000014",
              "nPagSubpoliza":"0000000",
              "nPagCertificado":"000000",
              "kPoliza":"0007000014",
              "kSubpoliza":"0000000",
              "kSuplemento":"000001",
              "kCertificado":"000000",
              "kVersion":"001"
            },{  
              "cEstado":"PE",
              "cSituacion":"DE",
              "fApunte":"02/06/2017",
              "iPrimaTotal":"1000,00",
              "iReajuste":"100,00",
              "cTipoApunte":"CA",
              "nPagPoliza":"0007000014",
              "nPagSubpoliza":"0000000",
              "nPagCertificado":"000000",
              "kPoliza":"0007000014",
              "kSubpoliza":"0000000",
              "kSuplemento":"000001",
              "kCertificado":"000000",
              "kVersion":"001"
            },{  
              "cEstado":"PE",
              "cSituacion":"DE",
              "fApunte":"02/07/2017",
              "iPrimaTotal":"1000,00",
              "iReajuste":"100,00",
              "cTipoApunte":"CA",
              "nPagPoliza":"0007000014",
              "nPagSubpoliza":"0000000",
              "nPagCertificado":"000000",
              "kPoliza":"0007000014",
              "kSubpoliza":"0000000",
              "kSuplemento":"000001",
              "kCertificado":"000000",
              "kVersion":"001"
            }*/                          
        ],
        "paginación":{  
           "kPoliza":"0007000014",
           "kSubpoliza":"0000000",
           "kSuplemento":"000001",
           "kCertificado":"000000",
           "kVersion":"001",
           "fApunte":"01/04/2017"
        }
     }      
    };
    
  }  




})();
