/**
 * Created by Fred on 9/24/2017.
 */
(function () {
    'use strict';
    /* global appModule */
    (appModule.lazy || appModule)
        .factory('tablasGeneralesSrv', ['HttpSrv', function ( HttpSrv) {  
          
          var url = '/api/js_listas_opciones_vida',
              config = {cache: true};

          function loadBusiness(){
            return HttpSrv.get(url + '/t000003', config);
          }

          function loadStates(){
            return HttpSrv.get(url + '/t000001', config);
          }          

          function loadSituations(){
            return HttpSrv.get(url + '/t000011',config);
          } 

          function loadFormaPago(){
            return HttpSrv.get(url + '/t00013l',config);
          }
          
          function loadMetodoReg(){
            return HttpSrv.get(url + '/t000012',config);
          }           

          function loadOrigen(){
            return HttpSrv.get(url + '/t000002',config);
          }            

          function loadTipoApunte(){
            return HttpSrv.get(url + '/t000004',config);
          }  
          
          function loadTipoOperacion(){
            return HttpSrv.get(url + '/t000005',config);
          }

          function loadTipoSoporte(){
            return HttpSrv.get(url + '/t01020l',config);
          }          

          return {
            loadBusiness: loadBusiness,
            loadStates: loadStates,
            loadSituations: loadSituations,
            loadFormaPago: loadFormaPago,
            loadMetodoReg: loadMetodoReg,
            loadOrigen: loadOrigen,
            loadTipoApunte: loadTipoApunte,
            loadTipoOperacion: loadTipoOperacion,
            loadTipoSoporte: loadTipoSoporte           
          };         

        }]);

})();
