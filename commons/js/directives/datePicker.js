/**
 * Created by Relvin on 9/17/2017.
 */
(function () {
    'use strict';
    /*global appModule */
    (appModule.lazy || appModule)
        .directive('datePicker', ['$window', function ($window) {
           return{
               restrict:'A',
               link:function(scope, element, attrs){
                   if(!$window.parent.isiPaint) {
                       setTimeout(function () {
                           element.datepicker({
                               showOn: "button",
                               buttonImage: "/commons/img/_IcoCalendario.png",
                               buttonImageOnly: true,
                               buttonText: "Calendar"
                           });
                       }, 1000);
                   }
               }
           }
        }]);

})();
