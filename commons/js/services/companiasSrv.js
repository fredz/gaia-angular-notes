/**
 * Created by Fred on 9/24/2017.
 */
(function () {
    'use strict';
    /* global appModule */
    (appModule.lazy || appModule)
        .factory('companiasSrv', ['HttpSrv', function ( HttpSrv) {  
          
          var url = '/api/listadoCompanias';

          function loadCompanies(){
            return HttpSrv.get(url);
          }       

          return {
            loadCompanies: loadCompanies
          }         

        }]);

})();
