/**
 * Created by Relvin on 9/16/2017.
 */
(function () {
    'use strict';

    /*global appModule */
    (appModule.lazy || appModule)
        .controller('BusquedaApuntesCtrl', ['$scope', 'usuarioSrv',      
          function ($scope, usuarioSrv) {  
             
            function init(){
              // init user life
              usuarioSrv.userLife().then(function(res){
                $scope.userLife = res;
              });
            }

            init();
        }]);
})();
