/**
 * Created by Fred on 9/29/2017.
 */
(function () {
    'use strict';
    /* global appModule */
    (appModule.lazy || appModule)
        .factory('alertaApuntesSrv', ['AlertsSrv', '$filter', function (AlertsSrv, $filter) { 

          var formAlerts;

          function showAlert(description, title) {
            var alert = {
                description: $filter('translate')(description),
                title: $filter('translate')(title)
            };
            return AlertsSrv.danger(alert, alert);
          }

          function showWarning(description, title) {
              var alert = {
                  description: $filter('translate')(description),
                  title: $filter('translate')(title)
              };
             return  AlertsSrv.danger(alert);
          }

          function showInfo(description, title) {
              var alert = {
                  description: $filter('translate')(description),
                  details: $filter('translate')(title)
              };
              return AlertsSrv.info(alert);
          }

          function cleanAlertas() {                        
            return AlertsSrv.clear();
          }

          function cleanFormAlert() {

            AlertsSrv.clear();

            if(formAlerts !== undefined){
              return formAlerts.clearErrors();
            }else{
              return;
            }
            
          }          

          function addAlerts(errors){            
            
            var title = 'Error',
                description = '';

            formAlerts = createAlert(title, description, errors);
            AlertsSrv.error(formAlerts);

          }

          function createAlert(title, description, errors) {
              var alert = new AlertsSrv.Alert(title, description);

              angular.forEach(errors, function (error) {
                  alert.addError(error.description, error.form, error.formControl);
              });

              return alert;
          }          


          return {
            showAlert: showAlert,
            showWarning: showWarning,
            showInfo: showInfo,
            cleanAlertas: cleanAlertas,
            addAlerts: addAlerts,
            cleanFormAlert: cleanFormAlert
          };         

        }]);

})();
