/**
 * Created by Fred on 9/24/2017.
 */
(function () {
  'use strict';
  /* global appModule */
  (appModule.lazy || appModule)
      .factory('modalConfirmacionSrv', ['$modal', function ( $modal) {  
        
        /* modal confirm */
        function alertConfirm(message, acceptFn, cancelFn){
            
          return $modal.open({
            templateUrl: '/commons/html/modalConfirmacionView.html', // loads the template
            backdrop: true, // setting backdrop allows us to close the modal window on clicking outside the modal window
            windowClass: 'modal-cancel-notes', // windowClass - additional CSS class(es) to be added to a modal window template
            controller: function ($scope, $modalInstance, $log) {

                $scope.message = message;
                
                $scope.save = function () {

                  // execute accept callback
                  if (acceptFn && typeof(acceptFn) === "function"){
                    acceptFn();
                  }
                  $modalInstance.dismiss('cancel'); 
                }
                $scope.cancel = function () {                  
                    
                  // execute cancel callback
                  if (cancelFn && typeof(cancelFn) === "function"){
                    cancelFn();
                  }
                  $modalInstance.dismiss('cancel');                      
                };
            }

          });//end of modal.open
        } // end of scope.open function  
        
        
        function confirmAltaApuntes(info, dataAltaApunte, altaFormAction){
          
          return $modal.open({
            templateUrl: '/commons/html/modalConfirmAltaApunteView.html', // loads the template
            backdrop: true, // setting backdrop allows us to close the modal window on clicking outside the modal window
            windowClass: 'modal-alta-apunte', // windowClass - additional CSS class(es) to be added to a modal window template
            controller: function ($scope, $modalInstance, $filter, LoadingSrv, apuntesSrv, $state) {

                $scope.altaApunteInfo = info;
                
                $scope.save = function () {            

                  LoadingSrv.set($filter('translate')('cargando'));

                  if(altaFormAction === 'createForm' || altaFormAction === 'copyForm'){

                    apuntesSrv.altaApunte(dataAltaApunte).then(function(){                    
                      afterEditAddAction();
                    });
                  }else if(altaFormAction === 'updateForm'){
                    
                    apuntesSrv.editarApunte(dataAltaApunte).then(function(){
                      afterEditAddAction();
                    });
                  }

                }

                function afterEditAddAction(){
                  $state.go('busquedaApuntes');
                  LoadingSrv.hide();
                  $modalInstance.dismiss('cancel');                  
                }

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel'); 
                };

                $scope.altaApunteOptions = {
                  'datatype': 'local',
                  'height': 'auto',
                  'editurl': 'clientArray',
                  'colNames': [                  
                      $filter('translate')('informacion')
                  ],
                  'colModel': [{
                    'name': 'info',
                    'index': 'info',
                    'align': 'left'
                    
                  }],                              
                  'rownumbers': false,
                  'cmTemplate': { sortable: false },
                  'rowNum': 100
                }
            }

          });//end of modal.open
        } // end of scope.open function         

        return {
          alertConfirm: alertConfirm,
          confirmAltaApuntes: confirmAltaApuntes
        };         

      }]);

})();
