/**
 * Created by Relvin on 9/19/2017.
 */
(function () {
    'use strict';
    /* global appModule */
    (appModule.lazy || appModule)
        .factory('apuntesSrv', ['rutaApuntesRecibo', 'HttpSrv', 'zeroConfigSrv', 'comunesSrv', 
        function (rutaApuntesRecibo, HttpSrv, zeroConfigSrv, comunesSrv) {  
          
          var url = '/api',
              URLApuntesRecibo = '';
          
          function getNotes(datas) {

            return zeroConfigSrv.getZeroConfig().then(function(data){
              
              URLApuntesRecibo = data.zeroConfig.URLApuntesRecibo;              
              return HttpSrv.post(url + "/apuntes", datas);
            })            
          }

          function calcularApunte(data){

            return zeroConfigSrv.getZeroConfig().then(function(data){
              
              URLApuntesRecibo = data.zeroConfig.URLApuntesRecibo;              
              return HttpSrv.post(url + "/calcularApunte", data);
            })
          }
          
        
          function altaApunte(data){

            return zeroConfigSrv.getZeroConfig().then(function(data){
              
              URLApuntesRecibo = data.zeroConfig.URLApuntesRecibo;              
              return HttpSrv.post(url + "/apunte", data);
            })
          }          

          function editarApunte(data){
            
            return zeroConfigSrv.getZeroConfig().then(function(data){
              
              URLApuntesRecibo = data.zeroConfig.URLApuntesRecibo;              
              return HttpSrv.post(url + "/editarApunte", data);
            })
          }
          
          function detalleApunte(data){

            return zeroConfigSrv.getZeroConfig().then(function(data){
              
              URLApuntesRecibo = data.zeroConfig.URLApuntesRecibo;              
              return HttpSrv.post(url + "/detalleApunte", data);
            })  
          }           
          
          return {
            getNotes: getNotes,
            calcularApunte: calcularApunte,
            editarApunte: editarApunte,
            altaApunte: altaApunte,
            detalleApunte: detalleApunte
          };         

        }]);

})();
