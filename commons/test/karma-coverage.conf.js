/*global JASMINE, JASMINE_ADAPTER, LOG_INFO */
(function (global) {
    'use strict';
    // Will be used to resolve files and exclude
    global.basePath = 'C:\GAIA_Front\workspace\src';

    // List of files/patterns to load in the browser. IMPORTANT! Loaded in order
    global.files = [
        JASMINE, // Required for testing
        JASMINE_ADAPTER, // Required for testing
        'commons/test/lib/vendor/jquery*.js', // Required by our fix in angular.js
        'commons/test/lib/vendor/angular.js', // Required for testing
        'commons/test/lib/angular-mocks.js', // Required for testing
        'commons/test/lib/namespace.js', // Required for testing
        'commons/test/lib/vendor/*.js', // Required for testing
        // 'commons/js/**/*.js', // Tested modules
        'commons/js/*.js', // Tested modules
        'commons/test/spec/**/*.js' // Tests
    ];

    // Test results reporter to use possible values: dots || progress
    global.reporter = 'progress';

    // Web server port
    global.port = 55504;

    // Cli runner port
    global.runnerPort = 55504;

    // Enable/disable colors in the output (reporters and logs)
    global.colors = true;

    // Level of logging. Possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
    global.logLevel = LOG_INFO;

    // Start these browsers. Currently available:, Chrome, ChromeCanary, Firefox, Opera, Safari, PhantomJS
    global.browsers = ['PhantomJS'];

    // Continuous Integration mode. If true, it capture browsers, run tests and exit
    global.singleRun = true;

    global.reporters = ['coverage', 'junit'];

    global.preprocessors = {
        'commons/js/*.js' : 'coverage'
    };

    global.coverageReporter = {
        type : 'lcovonly',
        dir : 'coverage/'
    };

    global.junitReporter = {
        outputFile : 'reports/TEST-results.xml'
    };

}(this));
